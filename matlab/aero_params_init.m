%%  Relative Drag Initialization Parameters

chief_geometry.area = 0.5; %    m^2
chief_geometry.drag_coeff = 2.2; %  m^2;
chief_geometry.mass = 100; %    kg
chief_geometry.beta = chief_geometry.area * chief_geometry.drag_coeff/ chief_geometry.mass;

dep_geometry.area = 0.0; %    m^2
dep_geometry.drag_coeff = 2.2; %  m^2;
dep_geometry.beta = dep_geometry.area * dep_geometry.drag_coeff;

atmo_density = 1e-13; % kg/m^3
chief_rad = 7500e3; %;m
mean_mot = 9.7202e-04;
[A_aero_pp, A_aero_pv, A_aero_vp, A_aero_vv, A_aero, v_aero] = A_aero_full(chief_geometry, dep_geometry, atmo_density, mean_mot, chief_rad);