function state_ecef = eci2ecef(time,theta0,state_eci)
wE = 7.29211585275553e-5;
wEvec = [0;0;wE];
theta = zeros(length(state_eci(1,:)));
state_ecef = zeros(size(state_eci));

for ii = 1:length(time)
    theta(ii) = theta0+wE*time(ii);
    state_ecef(1:3,ii) = rotz(theta(ii)*(180/pi))'*state_eci(1:3,ii);
    state_ecef(4:6,ii) = rotz(theta(ii)*(180/pi))'*state_eci(4:6,ii)-cross(wEvec,rotz(theta(ii)*(180/pi))'*state_eci(1:3,ii));
end