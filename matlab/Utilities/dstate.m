function [dvector,time] = dstate(time,vector,perts,params)

dvector = zeros(size(vector));
dvector(1:3,1) = vector(4:6,1);

mu = getparam(params,'mu');
muCRTBP = getparam(params,'muCRTBP');
J2 = getparam(params,'J2');
J3 = getparam(params,'J3');
CD = getparam(params,'CD');
Acs = getparam(params,'Acs');
msc = getparam(params,'msc');
JD0 = getparam(params,'JD0');
CR = getparam(params,'CR');
A2m = getparam(params,'A2m');
muS = getparam(params,'muS');
Cparams = getparam(params,'Cparams');
spacecraft = getparam(params,'spacecraft');

pert_ind = 1;
if any(strcmp(perts,'2body'))
    avec(:,pert_ind) = Twobody(vector,mu);
    pert_ind = pert_ind+1;
elseif any(strcmp(perts,'J2'))
    avec(:,pert_ind) = J2int(vector,mu,J2);
    pert_ind = pert_ind+1;
elseif any(strcmp(perts,'J3'))
    avec(:,pert_ind) = J3int(vector,mu,J2,J3);
    pert_ind = pert_ind+1;
end

if any(strcmp(perts,'CRTBP'))
    avec(:,pert_ind) = CRTBPint(vector,muCRTBP);
    pert_ind = pert_ind+1;
end

if any(strcmp(perts,'Drag'))
    avec(:,pert_ind) = Dragint(vector,{CD,msc,Acs});
    pert_ind = pert_ind+1;
end

if any(strcmp(perts,'SRP'))
    JD = JD0+(time/86400);
    avec(:,pert_ind) = SRPint(vector,CR,A2m,JD);
    pert_ind = pert_ind+1;
end

if any(strcmp(perts,'3body'))
    JD = JD0+(time/86400);
    avec(:,pert_ind) = Threebodyint(vector,muS,mu,JD);
    pert_ind = pert_ind+1;
end

if any(strcmp(perts,'Coulomb'))
    [aCl,aCf]  = ECI_CoulombAccel(vector,'rv',Cparams);
    if strcmp(spacecraft,'Follower')
        avec(:,pert_ind) = aCf;
    elseif strcmp(spacecraft,'Leader')
        avec(:,pert_ind) = aCl;
    end
    pert_ind = pert_ind+1;
end

%Add up the real dynamics of the system
dvector(4:6,1) = sum(avec,2);

end