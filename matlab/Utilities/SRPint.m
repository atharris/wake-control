function dstate = SRPint(state,CR,A2m,JD)

AU = 1.495978707e11;
state_ES = MeeusEphem('Earth',JD,'rv','EME2000');
Pphi = (1357/(299792458));
uvec = state(1:3)+state_ES(1:3);
u = norm(uvec);

dstate = Pphi*CR*A2m*AU^2*(uvec/u^3);