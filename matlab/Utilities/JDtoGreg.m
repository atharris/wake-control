function [Greg] = JDtoGreg(JD)

T_1900 = (JD-2415019.5)/365.25;
Year = 1900 + floor(T_1900);
LeapYrs = floor((Year-1990-1)*(.25));
Dayz = (JD-2415016.5)-((Year-1900)*(365)+LeapYrs);
LMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

if Dayz < 1
    Year = Year-1;
    LeapYrs = floor((Year-1990-1)*(.25));
    Dayz = (JD-2415016.5)-((Year-1900)*(365)+LeapYrs);
end

if mod(Year,4) == 0
    LMonth(2) = 29;
end

DayofYr = floor(Dayz);

ii = 0;
D = 0;
while D > DayofYr +1
    ii = ii + 1;
    D = sum(LMonth(1,1:ii));
end
Month = ii;
Day1 = DayofYr - D;
tau = (Dayz - DayofYr)*24;
h = floor(tau);
min = floor((tau-h)*60);
s = (tau - h - (min/60))*3600;

Greg = datetime(Year,Month,Day1,h,min,s);