function [state] = COE2RV(oe)

a = oe(1);
e = oe(2);
inc = oe(3);
raan = oe(4);
aop = oe(5);
M = oe(6);

mu = 3.986004415e14;
if e<1
    nu = Mean2True(M,e); % [rad/s] True Anomaly
elseif e>1
    nu = MeanH2True(M,e);
end
p = a*(1-e^2); % [m] {1x1} elliptical/hyperbolic semilatus rectum
r2d = 180/pi;

r_PQW = [(p*cos(nu))/(1+e*cos(nu));(p*sin(nu))/(1+e*cos(nu));0]; % [m] {3x1} position vector in PQW frame
v_PQW = [-sqrt(mu/p)*sin(nu);sqrt(mu/p)*(e+cos(nu));0]; % [m/s] {3x1} velocity vector in PQW frame

r_IJK = (rotz(-raan*r2d).'*rotx(-inc*r2d).'*rotz(-aop*r2d).'*r_PQW); % [m] {3x1} position vector in IJK frame
v_IJK = (rotz(-raan*r2d).'*rotx(-inc*r2d).'*rotz(-aop*r2d).'*v_PQW); % [m/s] {3x1} velocity vector in IJK frame

state = [r_IJK;v_IJK];

