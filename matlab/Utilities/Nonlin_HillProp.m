%% HW2 P6a

oe_c = [0.01,7500,45,20,30,20];
oe_d = oe_c + [0.0001,0,0.1,0,-0.01,0];

% Constants
mu = 3.986004415000000e+05; % [km^2/s^3] Earth Gravitational Constant
d2r = pi/180; % [rad/deg] Degree to Radian Conversion
p_c = oe_c(2)*(1-oe_c(1)^2); % [km] Chief Semilatus Rectum
n_c = sqrt(mu/oe_c(2)^3); % [rad/s] Chief Mean Motion
P = 2*pi/n_c;
dt = 1; % [s] Time Step
time = 0:dt:P*1'; % [s] Time Vector (Multiple Orbits)

% Vectors
rho = zeros(length(time),3);
drho = zeros(length(time),3);

% Convert Orbit Elements to Radians
oe_c(3:end) = oe_c(3:end)*d2r; % [~,km,rad,rad,rad,rad,rad] Chief
oe_d(3:end) = oe_d(3:end)*d2r; % [~,km,rad,rad,rad,rad,rad] Deputy

% Orbit Elements to ECI Conversion
[r_c,v_c] = COE2RV(oe_c(1),oe_c(2),oe_c(3),oe_c(4),oe_c(5),oe_c(6)); % [km,km/s] Chief
[r_d,v_d] = COE2RV(oe_d(1),oe_d(2),oe_d(3),oe_d(4),oe_d(5),oe_d(6)); % [km,km/s] Deputy

% Initial True Anomalies
f_c = Mean2True(oe_c(end),oe_c(1)); % [rad] Chief
df = sqrt(mu/p_c^3)*((1+oe_c(1)*cos(f_c))^2); % [rad/s] Chief True Anomaly Rate

% Convert to Hill Frame
[rho(1,:),drho(1,:)] = ECI2ChiefHill(r_c,v_c,r_d,v_d); % [km,km/s] Hill Frame Chief
%% 
for ii = 1:length(time)-1
    
    x = rho(ii,1);
    y = rho(ii,2);
    z = rho(ii,3);
    dx = drho(ii,1);
    dy = drho(ii,2);
    dz = drho(ii,3);
    
    % Orbit Radii
    rc = p_c/(1+oe_c(1)*cos(f_c)); % [km] Chief Radius
    rd = sqrt((rc+x)^2+y^2+z^2); % [km] Deputy Radius
    
    % Derivatives Generation
    df = sqrt(mu/p_c^3)*((1+oe_c(1)*cos(f_c))^2); % [rad/s] True Anomaly Rate
    drc = (sqrt(mu/p_c)*oe_c(1)*sin(f_c)); % [km/s] Chief Velocity
    
    % Acceleration Calculations
    ddx = (2*df*(dy-(y*(drc/rc))))+...
      (rho(ii,1)*df^2)+(mu/rc^2)-((mu/rd^3)*(rc+x)); % [km/s^2] ddX
    ddy = -2*df*(dx-(x*(drc/rc)))+...
      (y*df^2)-((mu/rd^3)*y); % [km/s^2] ddY
    ddz = -(mu/rd^3)*z; % [km/s^2] ddZ
    
    % Position and Velocity Update
    drho(ii+1,:) = drho(ii,:)+dt*[ddx,ddy,ddz]; % [km/s] Velocity Update
    rho(ii+1,:) = rho(ii,:)+dt*drho(ii+1,:)+...
      0.5*dt^2*[ddx,ddy,ddz]; % [km] Position Update
  
    % Mean and True Anomaly Updates
    oe_c(end) = oe_c(end)+dt*n_c; % [rad] Chief Mean Anomaly Update
    f_c = Mean2True(oe_c(end),oe_c(1)); % [rad] Chief True Anomaly Update    
    
end

%% HW2 P6b

for ii = 1:length(time)
    [rho2(ii,:),drho2(ii,:)] = HillFrame_Prop(r_c,v_c,r_d,v_d,dt);
    out_c = orbitprop('cart',[r_c,v_c],dt,'cart');
    out_d = orbitprop('cart',[r_d,v_d],dt,'cart');
    r_c = out_c(1:3);
    v_c = out_c(4:6);
    r_d = out_d(1:3);
    v_d = out_d(4:6);
end

figure
hold on
plot3(rho(:,1),rho(:,2),rho(:,3))
plot3(rho2(1:end-1,1),rho2(1:end-1,2),rho2(1:end-1,3),'r')
title('Hill Frame Relative Motion')
legend('Hill Frame Propogation','Inertial Frame Propogation')
xlabel('HCW X (km)')
ylabel('HCW Y (km)')
zlabel('HCW Z (km)')

figure
hold on
plot(time/P,rho-rho2)
title('Comparison of Hill and Inertial Propogators')
legend('HCW X','HCW Y','HCW Z')
xlabel('Time (Chief Period)')
ylabel('Difference Between Propogation Types (km)')


    