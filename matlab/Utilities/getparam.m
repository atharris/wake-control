function val = getparam(cell,identifier)

clength = length(cell);

for ii = 1:clength
    if find(strcmp(cell{ii}{1},identifier))
        val = cell{ii}{2};
        break
    else
        val = [];
    end
end
