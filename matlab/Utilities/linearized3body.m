function Amat = linearized3body(state,muS,muE,JD)

state_ES = MeeusEphem('Earth',JD,'rv','EME2000');
 
uvec = state_ES(1:3)+state(1:3);
u = norm(uvec);
uhat = uvec/u;
 
rvec = state(1:3);
r = norm(rvec);
rhat = rvec/r;

dadr = -(muS/u^3)*(eye(3)-3*(uhat*uhat'));
% da2dr = -(muE/r^3)*(eye(3)-3*(rhat*rhat'));

Amat = [zeros(3,3),eye(3);dadr,zeros(3,3)]+linearized2body(state,muE);