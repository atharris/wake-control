function [r_RSW,v_RSW,nu] = COE2RSW(mu,n,e,i,Omega,omega,M,u,lambda_true,omega_true)

if ~exist('u','var') % Check if u is an input
    u = u;
end

if ~exist('lambda_true','var') % Check if lambda_true is an input
    lambda_true = lambda_true;
end

if ~exist('omega_true','var') % Check if omega_true is an input
    omega_true = omega_true;
end
mu = 3.986004415000000e+05;
n = n*(360/86400); % [rad/s] {1x1} Mean Motion
a = (mu/n^2)^(1/3); % [km] {1x1} Semimajor Axis
n = n*(180/pi); % [deg/s] Mean Motion
nu = Ecc2True(Mean2Ecc(M,e),e); % [deg] {1x1}
p = a*(1-e^2); % % [km] {1x1} elliptical orbit semiparameter

r_PQW = [(p*cosd(nu))/(1+e*cosd(nu));(p*sind(nu))/(1+e*cosd(nu));0]; % [km] {3x1} position vector in PQW frame
v_PQW = [-sqrt(mu/p)*sind(nu);sqrt(mu/p)*(e+cosd(nu));0]; % [km/s] {3x1} velocity vector in PQW frame

r_RSW = rotz(nu).'*r_PQW; % [km] {3x1} position vector in IJK frame
v_RSW = rotz(nu).'*v_PQW; % [km/s] {3x1} velocity vector in IJK frame
R_RSW = norm(r_RSW);
V_RSW = norm(v_RSW);



