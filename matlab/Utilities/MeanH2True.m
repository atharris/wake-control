function nu = MeanH2True(M,e)

nu = Hyp2True(MeanH2Hyp(M,e),e);

end