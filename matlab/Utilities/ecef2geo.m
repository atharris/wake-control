function geo = ecef2geo(ecef)
    %Converts ECEF coordinates to geodetic coordinates, 
    %
    %Parameters
    %----------
    %X : an array of N ECEF coordinates with shape (3,N).
    %
    %Returns
    %-------
    %output : (3,N) array
    %    geodetic coordinates in degrees and meters (lat, lon, alt)
    %
    rf = 298.257223563; % Earth reciprocal flattening (1/f)
    a = 6378136.3;       % Earth semi-major axis (m)
    b = a - a / rf;     % Earth semi-minor axis derived from f = (a - b) / a
%     b = a; %Spherical Earth

    %! compute `lat`, `lon`, and `h` geodetic coordinates
    x = ecef(1,:);
    y = ecef(2,:);
    z = ecef(3,:);

    % iteratively derive N
    lat = atan2(z, sqrt(x.^2 + y.^2));
    h = 0; %z ./ sin(lat);
    d_h = 1.; d_lat = 1.;
    N = 0;

    timeout = 200;
    tol = 1e-15;
    while any(d_h > tol) || any(d_lat > tol)
        N = a^2 ./ (sqrt(a^2 * cos(lat).^2 + b^2 * sin(lat).^2));
        N1 = N * (b ./ a)^2;
    
        temp_h = sqrt(x.^2 + y.^2) ./ cos(lat) - N;
        temp_lat = atan2(z ./ (N1 + h), sqrt(x.^2 + y.^2) ./ (N + h));
        d_h = max(abs(h - temp_h));
        d_lat = max(abs(lat - temp_lat));

        h = temp_h;
        lat = temp_lat;
        timeout = timeout - 1;
        if timeout <= 0
            break;
        end
    end

    lon = atan2(y, x);
    lat = 180 / pi * lat;
    lon = 180 / pi * lon;

    geo = [lat; lon; h];

end
