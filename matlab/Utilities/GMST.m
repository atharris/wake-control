function GMST = GMST(yr,mo,d,h,min,s)

rtod = (180/pi);
UT = UT1(yr,mo,d,h,min,s)
UTS = s+min*60+h*3600
%GMST_h0 = 24110.54841+(8640184.812866*UT)+0.093104*(UT^2)-(6.2e-6)*(UT^3)
%GMST = mod(GMST_h0/240 + (rtod*7.2921158553e-5)*UTS,360);
GMST1 = 67310.54841+(876600*3600+8640184.812866)*UT+0.093104*UT^2-(6.2e-6)*UT^3
GMST = (mod(GMST1,-86400)/240)+360 % [deg]