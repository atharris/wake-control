function [time,state] = state_integrator(time,state0,perts,params,events)

if any(strcmp(events,'radius_condition'))
    Opt = odeset('Events', @radius_condition);
else
    Opt = [];
end

toleranceoptions = odeset('Reltol',1e-14,'Abstol',1e-16*ones(size(state0)));
Opt = {Opt,toleranceoptions};
[time,vecout] = ode45(@(t,y) dstate(t,y,perts,params),time,state0,toleranceoptions);
state = vecout';