function dstate = Nbodypert(state,stateNbody,mu)

r_rel = state(1:3)-stateNbody(1:3);
R_rel = norm(r_rel);

dstate = -(mu/R_rel^3)*r_rel;