function [r_RSW,v_RSW] = ECI2RSW(r_IJK,v_IJK)

[e,~,inc,raan,aop,M] = RV2COE(r_IJK,v_IJK);
r2d = 180/pi;

if  e < 0.0001 % IF Circular orbit
    nu = M;
else % IF Elliptical orbit
    nu = Mean2True(M,e);
end

r_RSW = (rotz((aop+nu)*r2d).'*rotx(inc*r2d).'*rotz(raan*r2d).'*r_IJK)-[norm(r_IJK);0;0];
v_RSW = rotz((aop+nu)*r2d).'*rotx(inc*r2d).'*rotz(raan*r2d).'*v_IJK;