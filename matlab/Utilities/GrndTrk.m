function [r_ECEF,Timing] = GrndTrk

global xx
global yy

mu = 3.986004415000000e+05
inc=51.6441;
e=0.0006691;
Omega=225.6859;
omega=42.3347;
Mean=63.3332;
n=15.54051996;

J_i = JulianDate(2016,10,5,22,6,8.64) % [days] {1x1} Epoch Julian Date
J_f = JulianDate(2016,10,11,01,00,00) % [days] {1x1} Target Julian Date
Prop = (J_f-J_i)*(86400); % [s] {1x1} Propogation Time
check =(J_f-J_i);

GMST_deg = GMST(2016,10,11,01,00,00); % [deg] {1x1} GMST at Target

rtod = 180/pi; % {1x1} Radians to Degrees Conversion
omega_E = rtod*7.2921158553e-5; % [deg/s] Earth Rotation Rate
dt = 60; % [s] {1x1} Time step
T = 3600*3; % [s] {1x1} 3 Hr Ground Track Calculation Time
it = T/dt; % {1x1} number of loops

n=n*(360/86400); % [deg/s] {1x1} Convert Mean Motion from [rev/day]
n_rad = n/rtod; % [rad/s] {1x1} Convert Mean Motion from [deg/s]
a = (mu/n_rad^2)^(1/3); % [km] {1x1} Orbit Semimajor Axis
M(1) = mod(Mean+(n*Prop),360) % [deg] {nx1} Mean Anomaly at Target
Timing(1) = J_f*86400; % [s] {nx1} 
[r(:,1),v(:,1),nu(1)] = COE2RV(mu,n,e,inc,Omega,omega,M(1)) 
% [km,km/s] [{3x1},{3x1}] Initial ECI position and velocities
[GLong(1),GLat(1),r_ECEF(:,1)] = eci2ecef(r(:,1),GMST_deg(1));
% [deg,deg] [{1x1},{1x1}] Initial Geocentric Longitude and Latitude
for i = 2:it
    Timing(i) = Timing(i-1)+dt; % [s] {nx1} Time Update
    M(i) = M(i-1) + n*dt; % [deg] {nx1} Mean Motion Update
    [r(:,i),v(:,i),nu(i)] = COE2RV(mu,n,e,inc,Omega,omega,M(i));
    % [km,km/s] [{3xn},{3xn}] ECI position and velocity Update
    GMST_deg = GMST_deg + omega_E*dt; % GMST Angle Update
    [GLong(i),GLat(i),r_ECEF(:,i)] = eci2ecef(r(:,i),GMST_deg);
    % [deg,deg] [{1x1},{1x1}] Geocentric Longitude and Latitude Update
end

%figure
%hold on
%plot(GLong,GLat,'ro')
%plot(xx,yy)
%xlabel('Longitude')
%ylabel('Latitude')
%title('Ground Track for HW6 Problem 1')
%axis([-180 180 -90 90])

