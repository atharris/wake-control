function stateH = ECI2ChiefHill(statel,statef)

r2d = 180/pi;
mu = 3.986004415000000e+14;
oe = RV2COE(statel);

a = oe(1);
e = oe(2);
inc = oe(3);
raan = oe(4);
aop = oe(5);
M = oe(6);

r_c = statel(1:3);
v_c = statel(4:6);
r_d = statef(1:3);
v_d = statef(4:6);

if  e < 0.0001 % IF Circular orbit
    nu = M;
else % IF Elliptical orbit
    nu = Mean2True(M,e);
end
rc = norm(r_c);
dnu = norm(cross(r_c,v_c)/rc^2);
rho_N = r_d-r_c;
drho_N = v_d-v_c;

HN = rotz((aop+nu)*r2d)'*rotx(inc*r2d)'*rotz(raan*r2d)';

rho_H = (HN*rho_N);

stateH = [(HN*rho_N);((HN*drho_N)-cross([0;0;dnu],rho_H))];