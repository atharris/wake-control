function Amat = A2body(state,mu)

r = norm(state(1:3));

Amat = [zeros(3,3),eye(3);(mu/(r^3))*eye(3),zeros(3,3)];