function Hyp = True2Hyp(nu,e)

H_s = (sin(nu)*sqrt(e^2-1))/(1+e*cos(nu));
H_c = (e+cos(nu))/(1+e*cos(nu));

Hyp = atan2(H_s,H_c);

if Hyp < 0
    Hyp = Hyp + 2*pi;
else
    Hyp;
end