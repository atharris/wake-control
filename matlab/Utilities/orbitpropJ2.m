function orbitpropJ2(inputform,initcoords,time,NumberofSats)
set(0,'defaulttextinterpreter','latex')
% Units are km,s,rad

J2 = 0.0010826269;
rE = 6.378e3;
mu = 3.986004415000000e+05;
d2r = (pi/180);

% Setup time vector

dt = 1e1;
t = 0:dt:time;

% Initialize variables

raan = zeros(length(t),NumberofSats);
aop = raan;
M = raan;
E = raan;
H = raan;
r = zeros(length(t),3,NumberofSats);
v = r;
h = r;
draan = zeros(NumberofSats);
daop = draan;
dM = draan;
ecc = draan;
sma = draan;
inc = draan;
p = draan;
n = draan;
prod = draan;

for jj = 1:NumberofSats
    
if strcmp('cart',inputform) == 1
    r(1,:,jj) = initcoords(1:3,jj);
    v(1,:,jj) = initcoords(4:6,jj);
    oe = RV2COE(r(1,:,jj),v(1,:,jj));
    ecc(jj) = oe(1);
    sma(jj) = oe(2);
    inc(jj) = oe(3);
    raan(1,jj) = oe(4);
    aop(1,jj) = oe(5);
    M(1,jj) = oe(6);
elseif strcmp('oe',inputform) == 1
    ecc(jj) = initcoords(1,jj);
    sma(jj) = initcoords(2,jj);
    inc(jj) = initcoords(3,jj)*d2r;
    raan(1) = initcoords(4,jj)*d2r;
    aop(1) = initcoords(5,jj)*d2r;
    M(1) = initcoords(6,jj)*d2r;
    [r(1,:,jj),v(1,:,jj)] = COE2RV(ecc(jj),sma(jj),inc(jj),raan(1,jj),aop(1,jj),M(1,jj));
end


h(1,:,jj) = cross(r(1,:,jj),v(1,:,jj));
H(1,jj) = norm(h(1,:,jj));
E(1,jj) = 0.5*norm(v(1,:,jj))^2-mu/norm(r(1,:,jj));
p(jj) = sma(jj)*(1-ecc(jj)^2);
n(jj) = sqrt(mu/sma(jj)^3);
prod(jj) = J2*n(jj)*(rE/p(jj))^2;

for ii = 2:length(t)
    draan(jj) = -1.5*prod(jj)*cos(inc(jj));
    daop(jj) = 0.75*prod(jj)*((5*cos(inc(jj)))^2-1);
    dM(jj) = 0.75*prod(jj)*sqrt(1-ecc(jj)^2)*((3*cos(inc(jj)))^2-1);
    raan(ii,jj) = raan(ii-1,jj)+draan(jj)*dt;
    aop(ii,jj) = aop(ii-1,jj)+daop(jj)*dt;
    M(ii,jj) = mod(M(ii-1,jj) + dM(jj)*dt + n(jj)*dt,2*pi);
    [r(ii,:,jj),v(ii,:,jj)] = COE2RV(ecc(jj),sma(jj),inc(jj),raan(ii,jj),aop(ii,jj),M(ii,jj));
    h(ii,:,jj) = cross(r(ii,:,jj),v(ii,:,jj));
    H(ii,jj) = norm(h(ii,:,jj));
    E(ii,jj) = 0.5*norm(v(ii,:,jj))^2-mu/norm(r(ii,:,jj));
end
end

figure
plot(t,squeeze(h(:,1,1)),t,squeeze(h(:,2,1)),t,squeeze(h(:,3,1)),t,H(:,1))
title('Angular Momentum Conservation')
xlabel ('Time ($s$)')
ylabel('Angular Momentum ($\frac{km^2}{s}$)')
legend('h_I','h_J','h_K','h')
set(gca,'FontSize',18)
set(gcf,'PaperOrientation','landscape');

figure
plot(t,E(:,jj))
title('Energy Conservation')
xlabel ('Time ($s$)')
ylabel('Specific Energy ($\frac{J}{kg}$)')
set(gca,'FontSize',18)
set(gcf,'PaperOrientation','landscape');

figure
plot3(r(:,1,jj),r(:,2,jj),r(:,3,jj))
title('J2 Perturbed Orbit Depiction')
xlabel('x($km$)')
ylabel('y($km$)')
zlabel('z($km$)')
set(gca,'FontSize',18)
set(gcf,'PaperOrientation','landscape');