function Amat = linearized2body(state,mu)

rvec = state(1:3);
r = norm(rvec);
rhat = rvec/r;

dadr = -(mu/r^3)*(eye(3)-3*(rhat*rhat'));

Amat = [zeros(3,3),eye(3);dadr,zeros(3,3)];