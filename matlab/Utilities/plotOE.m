function plotOE(time,oe)

a = oe(:,1);
e = oe(:,2);
inc = oe(:,3);
raan = oe(:,4);
aop = oe(:,5);
M = oe(:,6);

mu = 3.986004415e14;

period = 2*pi*sqrt(a.^3/mu);

labelstr = {'SMA','ECC','INC','RAAN','AOP','MA'};
ylabstr = {'m','','rad','rad','rad','rad'};

for ii = 1:6
    subplot(3,2,ii)
    plot(time./period,oe(ii,:))
    title(labelstr{ii})
    ylabel(ylabstr{ii})
end