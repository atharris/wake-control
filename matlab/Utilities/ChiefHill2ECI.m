function statef = ChiefHill2ECI(statel,stateH)

r2d = 180/pi;
mu = 3.986004415000000e+014;
oe = RV2COE(statel);

a = oe(1);
e = oe(2);
inc = oe(3);
raan = oe(4);
aop = oe(5);
M = oe(6);

r_c = statel(1:3);
v_c = statel(4:6);
rho_H = stateH(1:3);
drho_H = stateH(4:6);

if  e < 0.0001 % IF Circular orbit
    nu = M;
else % IF Elliptical orbit
    nu = Mean2True(M,e);
end
rc = norm(r_c);
dnu = norm(cross(r_c,v_c)/rc^2);
wNH = [0;0;dnu];

NH = (rotz((aop+nu)*r2d)'*rotx(inc*r2d)'*rotz(raan*r2d)')';

statef = [(NH*rho_H)+r_c;(NH*(drho_H+cross(wNH,rho_H)))+v_c];
