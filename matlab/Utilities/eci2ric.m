function B = eci2ric(state)

rvec = state(1:3);
r = norm(rvec);
vvec = state(4:6);
hvec = cross(rvec,vvec);
h = norm(hvec);

R = rvec/r;
C = hvec/h;
I = cross(C,R);

B = [R';I';C'];