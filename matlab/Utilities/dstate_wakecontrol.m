function [dvector] = dstate_wakecontrol(time,vector,params,inputs)

leader = vector(1:6);
follower = vector(7:12);

inputs.rf = follower-leader;
inputs.chief_rad = norm(vector(1:3));
inputs.mean_mot = sqrt(params.mu/inputs.chief_rad^3);

[~, ~, ~, ~, A_aero, v_aero] = A_aero_full(inputs.chief_geometry, inputs.dep_geometry, inputs.atmo_density, inputs.mean_mot, inputs.chief_rad);
inputs.ad = max(abs(v_aero));

dvector = zeros(size(vector));
dvector(1:3,1) = leader(4:6,1);
dvector(7:9,1) = follower(4:6,1);

CDl = params.leader.CD;
mscl = params.leader.m;
Acsl = params.leader.Acs;
CDf = params.follower.CD;
mscf = params.follower.m;
Acsf = params.follower.Acs;
mu = params.mu;
rhoATM = params.rhoATM;

a2l = Twobody(leader,mu);
aDl = Dragint(leader,{CDl,mscl,Acsl,rhoATM});

aDf = Dragint(follower,{CDf,mscf,Acsf});
a2f = Twobody(follower,mu);

inputs.Vnom = inputs.Vnom0+inputs.LQR_Gain*(inputs.rf-inputs.rfnom);
[aCl,aCf]  = ECI_CoulombAccel(vector,inputs);

dvector(4:6,1) = a2l + aDl + aCl;
dvector(10:12,1) = a2f + aDf + aCf;

accelerations = [norm(a2l);norm(aDl);norm(aCl);norm(a2f);norm(aDf);norm(aCf)];