function ecef = geo2ecef(geo)
    %Converts geodetic coordinates to ECEF coordinates
    %
    %Parameters
    %----------
    %geo : array of shape (3,N)
    %    geodetic coordinates (lat, lon, alt)
    %
    %Returns
    %-------
    %output : array of shape (3,N)
    %    ECEF coordinates
    %
    a = 6378.1363e3;  % Earth semi-major axis (km)
    rf = 298.257223563; % Reciprocal flattening (1/f)
%     b = a * (rf - 1) / rf; % Earth semi-minor axis derived from f = (a - b) / a
    b=a; %Spherical Earth Approximation
    wEvec = repmat([0;0;7.29211585275553e-005],[1,length(geo(1,:))]);
    lat = pi / 180 * geo(1,:);
    lon = pi / 180 * geo(2,:);
    h = geo(3,:);

    N = (a^2./ sqrt(a^2 * cos(lat).^2 + b^2 * sin(lat).^2));
    N1 = N * (b / a)^2;

    x = (N + h) .* cos(lat) .* cos(lon);
    y = (N + h) .* cos(lat) .* sin(lon);
    z = (N1 + h) .* sin(lat);
    
    rvec = [x; y; z];
    vvec = cross(wEvec,rvec);
    ecef = [rvec;vvec];
end
