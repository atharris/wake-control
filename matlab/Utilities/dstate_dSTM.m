function [dvector,time] = dstate_dSTM(time,vector,statelength,perts,statedefn,params)

if any(strcmp(statedefn,'consider'))
    considerpos = find(strcmp(statedefn,'consider'));
    q = length(statedefn)-considerpos;
    N = statelength+q;
    dvector = zeros(statelength+N^2,1);
else
    dvector = zeros(statelength+statelength^2,1);
end

state = vector(1:statelength);
dvector(1:6) = dstate(time,vector(1:6),perts,params);

%Use section below if station positions are in ECI.

% %Populate non-zero elements dealing with Stations
% if any(strcmp(statedefn,'Stations'))
% %Find the position of the Stations within statedefn to indicate position in
% %Params and state.
%     Stations_ind = find(strcmp(statedefn,'Stations'));
%     Station1_startind = 6+Stations_ind;
%     Station1_stopind = 6+Stations_ind+2;
%     Station2_startind = Station1_stopind+1;
%     Station2_stopind = Station2_startind+2;
%     Station3_startind = Station2_stopind+1;
%     Station3_stopind = Station3_startind+2;
%     
%     dvector(Station1_startind:Station3_stopind) = [cross(Params{Stations_ind}{1},vector(Station1_startind:Station1_stopind));cross(Params{Stations_ind}{1},vector(Station2_startind:Station2_stopind));cross(Params{Stations_ind}{1},vector(Station3_startind:Station3_stopind))];
% end
if any(strcmp(statedefn,'DMC'))
    DMC_ind = findincell(params,'DMC');
    DMCinstate = length(state)-2;
    avec(:,pert_ind) = state(DMCinstate:end);
    dvector(DMCinstate:DMCinstate+2) = diag(params{DMC_ind}{2})*state(DMCinstate:end);
    dSTMDMC = DMC(time,statelength,buildA(time,state(1:end-3),perts,statedefn,params),reshape(vector(statelength+1:end),statelength,statelength),params{DMC_ind}{2});
    dvector(statelength+1:end) = reshape(dSTMDMC,statelength^2,1);
elseif any(strcmp(statedefn,'consider'))
    STM = reshape(vector(statelength+1:end),N,N);
    Phi = STM(1:statelength,1:statelength);
    Theta = STM(1:statelength,statelength+1:statelength+q);
    dPhi = buildA(time,state,perts,statedefn,params)*Phi;
    dTheta = buildA(time,state,perts,statedefn,params)*Theta+buildB(time,state,perts,statedefn,params);
    dSTM = [dPhi,dTheta;zeros(q,N)];
    dvector(statelength+1:end) = reshape(dSTM,N^2,1);
else
    dvector(statelength+1:end) = reshape(buildA(time,state,perts,statedefn,params)*reshape(vector(statelength+1:end),statelength,statelength),statelength^2,1);
end
    
    
    
    

end