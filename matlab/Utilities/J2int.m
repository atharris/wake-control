function dstate = J2int(state,mu,J2)

req = 6378136.3;
rvec = state(1:3);
kvec = [0;0;1];
r = norm(rvec);
sinphi = rvec(3)/r;

% dstate = -(mu/r^3)*rvec-1.5*J2*(mu*req^2/r^4)*[(1-5*sinphi^2)*(rvec(1)/r);...
%     (1-5*sinphi^2)*(rvec(2)/r);(3-5*sinphi^2)*(rvec(3)/r)];

% [5*((7*sinphi^3)-3*(sinphi^2))*rvec(1)/r;...
%     5*((7*sinphi^3)-3*(sinphi^2))*rvec(1)/r;...
%     3*(10*sinphi^2-(35/3)*sinphi^4-1)*sinphi];

dstate = -(mu/r^3)*rvec-((3*mu*req^2*J2)/(2*r^5))*((1-5*sinphi^2)*rvec+2*r*sinphi*kvec);
end