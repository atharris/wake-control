function [rho,drho] = HillFrame_Prop(rc0,vc0,rd0,vd0,dt)

out_c = orbitprop('cart',[rc0,vc0],dt,'cart');
out_d = orbitprop('cart',[rd0,vd0],dt,'cart');

mu = 3.986004415000000e+05;

rc_N = out_c(1:3);
vc_N = out_c(4:6);
rd_N = out_d(1:3);
vd_N = out_d(4:6);

[rho,drho] = ECI2ChiefHill(rc_N,vc_N,rd_N,vd_N);

end