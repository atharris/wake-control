function LROE = ChiefHill2LROE(n,time,rho,drho)

A1 = -((3*n*rho(1)+2*drho(2))*cos(n*time)+drho(1)*sin(n*time))/n;
A2 = ((3*n*rho(1)+2*drho(2))*sin(n*time)-drho(1)*cos(n*time))/n;
xoff = 4*rho(1)+((2*drho(2))/n);
yoff = -((2*drho(1))/n)+rho(2)+(6*n*rho(1)+3*drho(2))*time;
B1 = rho(3)*cos(n*time)-((drho(3)*sin(n*time))/n);
B2 = -rho(3)*sin(n*time)-((drho(3)*cos(n*time))/n);

LROE = [A1,A2,xoff,yoff,B1,B2];