function oe = RV2COE(state)

r = state(1:3,:);
v = state(4:6,:);

%Inertial Coordinate Frame%

I = repmat([1,0,0],length(r(1,:)),1)';
J = repmat([0,1,0],length(r(1,:)),1)';
K = repmat([0,0,1],length(r(1,:)),1)';

%Important Physical Constants$

mu = 3.98600432896939e14;
rd = 180/pi; % radians to degrees
r_n = vecnorm(r); % [m] position norm
v_n = vecnorm(v); % [m/s] velocity magnitude
h = cross(r,v); % [m^2/s] {3x1} orbit angular momentum vector
h_n = vecnorm(h); % [m^2/s] {1x1} angular momentum magnitude
n = cross(K,h); % [m^2/s] {3x1} node vector
n_n = vecnorm(n); % [m^2/s] {1x1} node vector magnitude
e_vec = (((v_n.^2-(mu./r_n)).*r)-(dot(r,v).*v))/mu; % [unitless] {3x1} eccentricity vector
e = vecnorm(e_vec); % [unitless] {1x1} eccentricity magnitude
E = (v_n.^2/2)-(mu./r_n); % [J/kg] {1x1} orbit energy
a = -mu./(2*E); % [m] {1x1} elliptical orbit semimajor axis
p = a.*(1-e.^2); % [m] {1x1} elliptical orbit semiparameter

n_mm = sqrt(mu./a.^3); % [rad/s] {1x1} mean motion

%Orbital Elements$

inc = acos(dot(K,h)./h_n); % [rad] inclination

if e > 0.0001 & abs(inc) > 0.01 % IF not circular or equatorial
    
    raan = acos(dot(n,I)./n_n); % [rad] right ascension of the ascending node
    
    aop = acos(dot(n,e_vec)./(n_n.*e)); % [rad] argument of perigee

    nu = acos(dot(e_vec,r)./(e.*r_n)); % [rad] true anomaly
    
    if e < 1    
        M = True2Mean(nu,e); % [rad] mean anomaly
    elseif e > 1
        M = True2MeanH(nu,e);
    end
    
elseif e > 0.0001 && abs(inc) < 0.01 % IF Elliptical Equatorial
    
    raan = 0; % [rad] right ascension of the ascending node
    
    aop = acos(e_vec(1)/e); % [rad] true longitude of perigee
    if e_vec(2)<0
        aop = 2*pi - aop; % [rad] true longitude of perigee correction
    end
    
    nu = acos(dot(e_vec,r)/(e*r_n)); % [rad] true anomaly
    if dot(r,v)<0
        nu = 2*pi-nu; % [rad] true anomaly correction
    end
    
    M = True2Mean(nu,e); % [rad] mean anomaly
    
elseif e < 0.0001 && abs(inc) > 0.001 % IF Circular Inclined
    
    e=0;
    
    raan = acos(dot(n,I)/n_n); % [rad] right ascension of the ascending node
    if dot(n,J)<0
        raan = 2*pi-raan; % [rad] right ascension of the ascending node correction
    end
    
    aop = 0; % [rad] argument of perigee
    
    nu = acos(dot(n,r)/(n_n*r_n)); % [rad] argument of latitude
    if r(3)<0
        nu = 2*pi - nu; % [rad] argument of latitude correction
    end
    
    M = nu; % [rad] mean anomaly
    
elseif e < 0.0001 && abs(inc) < 0.001 % IF Circular Equatorial
    
    raan = 0; % [rad] right ascension of the ascending node
    
    aop = 0; % [rad] argument of perigee
    
    nu = acos(r(1)/r_n); % [rad] true longitude

    if r(2) < 0
        nu = 2*pi - nu; % [rad] true longitude correction
    end
    
    M = nu; % [rad] mean anomaly
    
end

% length(a)
% length(e)
% length(inc)
% length(raan)
% length(aop)
% length(M)
oe = [a;e;inc;raan;aop;M];
end