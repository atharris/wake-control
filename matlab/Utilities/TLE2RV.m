function TLE2RV(mu,M,e,i,Omega,omega,n,u,lambda_true,omega_true)

n = n*(1/(24*3600))*(2*pi) % [rad/s] orbit mean motion
a = (mu/n^2)^(1/3) % [km] orbit mean semimajor axis
M = (n*(180/pi)*3600)+M-360
if e<0.001
    nu = M;
    E = M;
else
    E(1) = 0;
    E(2) = M;
    i=2;
    while abs(E(i)-E(i-1))>10e-6
        E(i+1) = E(i)-((M-E(i)+e*sind(E(i)))/(e*cosd(E(i))-1));
        i = i+1;
    end
    i
    E = E(i)
    nus = (sind(E)*sqrt(1-e^2))/(1-e*cosd(E));
    nuc = (cosd(E)-e)/(1-e*cosd(E));
    nu = atan2d(nus,nuc)
end

if ~exist('u','var') % Check if u is an input
    u = u;
end

if ~exist('lambda_true','var') % Check if lambda_true is an input
    lambda_true = lambda_true;
end

if ~exist('omega_true','var') % Check if omega_true is an input
    omega_true = omega_true;
end


one=2;
two=3;
three=1;

if e<10^(-10) && i<20 % Check if circular equatorial
    lambda_true = Omega + omega + M;
    omega = 0;
    Omega = 0;
    nu = lambda_true;
    one = 1;
end

if e<10^(-10) && 10<i<170 % Check if circular inclined
    u = nu+omega;
    omega = 0;
    nu = u;
    two = 2;
end

if e>(10^-10) && i<20 % Check if elliptical equatorial
    omega_true = Omega + omega;
    Omega = 0;
    omega = omega_true;
    three = 3;
end

if e~=1
    p = a*(1-e^2); % % [km] {1x1} elliptical orbit semiparameter
else
    p = h_n^2/mu; % [km] {1x1} parabolic orbit semiparameter
end

r_PQW = [(p*cosd(nu))/(1+e*cosd(nu));(p*sind(nu))/(1+e*cosd(nu));0]; % [km] {3x1} position vector in PQW frame
v_PQW = [-sqrt(mu/p)*sind(nu);sqrt(mu/p)*(e+cosd(nu));0]; % [km/s] {3x1} velocity vector in PQW frame

r_IJK = rotz(-Omega).'*rotx(-i).'*rotz(-omega).'*r_PQW % [km] {3x1} position vector in IJK frame
v_IJK = rotz(-Omega).'*rotx(-i).'*rotz(-omega).'*v_PQW % [km/s] {3x1} velocity vector in IJK frame

if one == 1
    display('Orbit is circular equatorial')
end

if two == 2
    display('Orbit is circular inclined')
end

if three == 3
    display('Orbit is elliptical equatorial')
end