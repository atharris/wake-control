function [doe] = GaussVar(oe,u)

mu = 3.986004415000000e+05;
p = oe(1)*(1-oe(2)^2);
h = sqrt(mu*p);
theta = oe(5)+oe(6);
f = Mean2True(oe(6),oe(2))
r = p/(1+oe(2)*cos(f));

doe(1) = (2*oe(1)^2/h)*((oe(2)*sin(f)*u(1))+(p/r)*u(2);
doe(2) = (1/h)*