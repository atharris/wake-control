function xhat = backsub(b,R)

n = length(b);
xhat = zeros(n,1);
xhat(n) = b(n)/R(n,n);

for ii = n-1:-1:1
    jj = ii+1;
    xhat(ii,1) = (b(ii)-sum(R(ii,jj:n).*xhat(jj:n,1)'))/R(ii,ii);
end