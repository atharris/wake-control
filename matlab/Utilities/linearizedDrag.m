function Amat = linearizedDrag(state,DParams)

%Drag Model Parameters
re = 6378136.3;
rho0 = 3.614e-13;
r0 = 700000+re;
H = 88667;

%Relative Velocity Calculation (Assumes Co-Rotating Atmosphere)
wE = [0;0;7.2921158553E-5];
vATM = cross(wE,state(1:3));
vRel = vATM-state(4:6);
vR = norm(vRel);
vRelhat = vRel/vR;
rhat = state(1:3)/norm(state(1:3));
%Density Model
rho = rho0*exp(-(norm(state(1:3))-r0)/H);

dadr = 0.5*rho*((DParams{1}*DParams{3})/DParams{2})*((-(vR/H)*vRel*rhat')+vRelhat*vRel'*tilde(wE)+vR*tilde(wE));
dadv = 0.5*rho*((DParams{1}*DParams{3})/DParams{2})*(-((vRel*vRel')/vR)-eye(3)*norm(vRel));

Amat = [zeros(3,3),eye(3);dadr,dadv];