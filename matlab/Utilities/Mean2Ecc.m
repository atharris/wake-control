function Ecc = Mean2Ecc(M,e)
E0 = M+(100*(pi/180));
E = M;
i=2;
while any(abs(E-E0)>10^-12)
    E0 = E;
    E=E0+((M-E0+e.*(sin(E0)))./(1-e.*cos(E0)));
end
Ecc=mod(E,2*pi);

for ii = 1:length(Ecc)
    if Ecc(ii) < 0
        Ecc(ii) = Ecc(ii) + 2*pi;
    end
end

