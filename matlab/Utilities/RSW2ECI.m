function [r_RSW,v_RSW] = RSW2ECI(r_IJK,v_IJK)

mue = 3.986004415000000e+05;
[n,e,inc,raan,aop,M] = RV2COE(mue,r_IJK,vec_IJK);
vec_RSW = rotz(-raan).'*rotx(-inc).'*rotz(-aop).'*rotz(-nu).'*vec_IJK;