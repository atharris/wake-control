function [time,state,STM] = state_STM_integrator(time,state0,STM0,perts,statedefn,params,events)

statelength = length(state0);

if any(strcmp(statedefn,'consider'))
    considerpos = find(strcmp(statedefn,'consider'));
    q = length(statedefn)-considerpos;
    N = statelength+q;
else
    N = statelength;
end

vecin = [state0;reshape(STM0,N^2,1)];

if nargin == 7 && any(strcmp(events,'radius_condition'))
    disp('Using Radius Condition')
    Opt = odeset('Events', @(t,y) radius_condition);
else
    Opt = [];
end

toleranceoptions = odeset('Reltol',1e-14,'Abstol',1e-16*ones(length(vecin),1));
Opt = {Opt,toleranceoptions};

[time,vecout] = ode45(@(t,y) dstate_dSTM(t,y,statelength,perts,statedefn,params),time,vecin,toleranceoptions);
state = vecout(:,1:statelength)';
STM = reshape(vecout(:,statelength+1:end)',N,N,length(vecout(:,1)));