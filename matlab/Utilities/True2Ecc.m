function Ecc = True2Ecc(nu,e)

E_s = (sin(nu).*sqrt(1-e.^2))./(1+e.*cos(nu));
E_c = (e+cos(nu))./(1+e.*cos(nu));

Ecc = atan2(E_s,E_c);

if Ecc < 0
    Ecc = Ecc + 2*pi;
else
    Ecc;
end