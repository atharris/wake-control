function [BR,BT,b,DCM] = getBplane_DCM(state)

mu = 3.98600432896939e14;

r = state(1:3);
v = state(4:6);
oe = RV2COE(state);

V = norm(v);
R = norm(r);
Nhat = [0;0;1];

evec = (1/mu)*((V^2-(mu/R))*r-(dot(r,v)*v));

hvec = cross(r,v);
h = norm(hvec);

b = abs(oe(1))*sqrt(oe(2)^2-1);

Phat = evec/norm(evec);
What = hvec/h;
Qhat = cross(What,Phat);

Shat = v/V;
That = cross(Shat,Nhat)/norm(cross(Shat,Nhat));
Rhat = cross(Shat,That);

Bvec = b*cross(Shat,What);

BR = dot(Bvec,Rhat);
BT = dot(Bvec,That);
B = [BR,BT];

DCM = [Shat,That,Rhat]';