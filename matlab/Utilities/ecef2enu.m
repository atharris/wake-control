function enu = ecef2enu(ecef_ref, ecef_obj)
    %Converts ECEF coordinates to observer-relative ENU coordinates.
    %
    %Parameters
    %----------
    %ecef_ref : array of shape (3,1)
    %    observer ECEF coordinate
    %ecef_obj : array of shape(3,N)
    %    object ECEF coordinates
    %
    %Returns
    %-------
    %output : array of shape(3,N)
    %    The east-north-up coordinates
    %

    % get the lat and lon of the user position
    geo = ecef2geo(ecef_ref);
    lat = pi / 180 * geo(1,:);
    lon = pi / 180 * geo(2,:);

    % create the rotation matrix
    d_ecef = ecef_obj - repmat(ecef_ref,[1,length(ecef_obj)]);
    dx = d_ecef(1,:);
    dy = d_ecef(2,:);
    dz = d_ecef(3,:);
    e = -sin(lon) .* dx + cos(lon) .* dy;
    n = -sin(lat) .* cos(lon) .* dx - sin(lat) .* sin(lon) .* dy + cos(lat) .* dz;
    u = cos(lat) .* cos(lon) .* dx + cos(lat) .* sin(lon) .* dy + sin(lat) .* dz;
    enu = [e; n; u];
end
