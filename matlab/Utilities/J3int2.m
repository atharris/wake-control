function dstate = J3int2(state,mu,J2,J3)

req = 6378136.3;
rvec = state(1:3);
kvec = [0;0;1];
r = norm(rvec);
sinphi = rvec(3)/r;

dstate = -(mu/r^3)*rvec-((3*mu*req^2*J2)/(2*r^5))*((1-5*sinphi^2)*rvec+2*r*sinphi*kvec)...
    -(0.5*((mu*req^3)/(r^6))*J3*((-35*sinphi^3+15*sinphi)*rvec+(15*r*sinphi^2-3*r)*kvec));
end