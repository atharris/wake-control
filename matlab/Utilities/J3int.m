function dstate = J3int(state,mu,J2,J3)

req = 6378136.3;
rvec = state(1:3);
kvec = [0;0;1];
r = norm(rvec);
sinphi = rvec(3)/r;

% a3 = 0.5*J3*((mu*req^3)/r^5)*[5*(7*sinphi^3-3*sinphi)*(rvec(1)/r);5*(7*sinphi^3-3*sinphi)*(rvec(2)/r)...
%     ;3*(1-10*sinphi^2+(35/3)*sinphi^4)];

a3 = -((mu*req^3)/(2*r^6))*J3*((-35*sinphi^3+15*sinphi)*rvec+(15*r*sinphi^2-3*r)*kvec);

dstate = J2int(state,mu,J2)+a3;
end