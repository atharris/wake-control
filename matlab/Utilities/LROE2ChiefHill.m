function [rho,drho] = LROE2ChiefHill(n,time,LROE)

x = LROE(1)*cos(n*time)-LROE(2)*sin(n*time)+LROE(3);
y = -2*LROE(1)*sin(n*time)-2*LROE(2)*cos(n*time)-1.5*n*time*LROE(3)+LROE(4);
z = LROE(5)*cos(n*time)-LROE(6)*sin(n*time);
dx = -LROE(1)*n*sin(n*time)-LROE(2)*n*cos(n*time);
dy = -2*LROE(1)*n*cos(n*time)+2*LROE(2)*n*sin(n*time)-1.5*n*LROE(3);
dz = -LROE(5)*n*sin(n*time)-LROE(6)*n*cos(n*time);
rho = [x,y,z];
drho = [dx,dy,dz];