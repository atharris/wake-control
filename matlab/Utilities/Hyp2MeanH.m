function M = Hyp2MeanH(H,e)

M=(e*sinh(H))-H;

if M < 0
    M = M + 2*pi;
else
    M;
end