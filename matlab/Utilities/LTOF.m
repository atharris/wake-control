function T = LTOF(state,mu)

r = state(1:3);
R = norm(r);
v = state(4:6);
V = norm(v);

evec = (1/mu)*((V^2-(mu/R))*r-dot(r,v)*v);
Phat = evec/norm(evec);
rhat = r/R;

nu = acos(rhat'*Phat);

oe = RV2COE(state);
p = oe(1)*(1-oe(2)^2);

f = acosh(1+((V^2)/mu)*(p/(1+oe(2)*cos(nu))));

T = (mu/V^3)*(sinh(f)-f);