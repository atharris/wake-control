function True = Ecc2True(E,e)

nu_s = (sin(E).*sqrt(1-e.^2))/(1-e.*cos(E));
nu_c = (cos(E)-e)./(1-e.*cos(E));

True = atan2(nu_s,nu_c);

if True < 0
    True = True + 2*pi;
end