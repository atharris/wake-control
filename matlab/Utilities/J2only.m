function dstate = J2only(state,mu,J2)

req = 6378136.3;
rvec = state(1:3);
kvec = [0;0;1];
r = norm(rvec);
sinphi = rvec(3)/r;

dstate = -((3*mu*req^2*J2)/(2*r^5))*((1-5*sinphi^2)*rvec+2*r*sinphi*kvec);
end