function dstate = J3only(state,J3val)

mu = 3.986004415000000e+14;
req = 6378136.3;
rvec = state(1:3);
kvec = [0;0;1];
r = norm(rvec);
sinphi = rvec(3)/r;

dstate = (0.5*((mu*req^3)/(r^6))*J3val*((-35*sinphi^3+15*sinphi)*rvec+(15*r*sinphi^2-3*r)*kvec));
end