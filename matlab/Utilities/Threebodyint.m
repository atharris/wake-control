function dstate = Threebodyint(state,muS,muE,JD)
 
state_ES = MeeusEphem('Earth',JD,'rv','EME2000');
 
uvec = state_ES(1:3)+state(1:3);
u = norm(uvec);
 
rESvec = state_ES(1:3);
rES = norm(rESvec);
 
rvec = state(1:3);
r = norm(rvec);

dstate = muS*(-(uvec/u^3)+(rESvec/rES^3))+Twobody(state,muE);
