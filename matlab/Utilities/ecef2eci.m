function state_eci = ecef2eci(time,theta0,state_ecef)
wE = 7.29211585275553e-5;
wEvec = [0;0;wE];
theta = zeros(size(time));
state_eci = zeros(length(state_ecef(:,1)),length(state_ecef(1,:)),length(time));

for jj = 1:length(time)
    theta(jj) = theta0+wE*time(jj);
    for ii = 1:length(state_ecef(1,:))
        state_eci(1:3,ii,jj) = rotz(theta(jj)*(180/pi))*state_ecef(1:3,ii);
        state_eci(4:6,ii,jj) = cross(wEvec,state_eci(1:3,ii,jj));
    end
end
%rotz(theta(jj)*(180/pi))*state_ecef(4:6,ii)