function state=orbitprop(inputform,initcoords,dt,outputform)

mu = 3.986004415e05;
d2r = (pi/180);
if strcmp('cart',inputform) == 1
    oe = RV2COE(initcoords);
    sma = oe(1);
    ecc = oe(2);
    inc = oe(3);
    raan = oe(4);
    aop = oe(5);
    M0 = oe(6);
elseif strcmp('oe',inputform) == 1
    sma = initcoords(1);
    ecc = initcoords(2);
    inc = initcoords(3);
    raan = initcoords(4);
    aop = initcoords(5);
    M0 = initcoords(6);
end

E = -(mu/(2*sma));

if E < 0
    n = sqrt(mu/(sma^3));
    M = mod(M0 + n*dt,2*pi);
elseif E > 0
    n = sqrt(mu/(-sma^3));
    M = mod(M0+n*dt,2*pi);
elseif abs(E) < 0.0001
    disp('Code not setup for parabolic orbits')
end
oe = [sma;ecc;inc;raan;aop;M];
if strcmp('cart',outputform) == 1
    state = COE2RV(oe);
elseif strcmp('oe',outputform) == 1
    state = [sma;ecc;inc;raan;aop;M];
end

end