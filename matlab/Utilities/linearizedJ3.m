function Amat = linearizedJ3(state,mu,J2,J3)

req = 6378136.3;
rvec = state(1:3);
kvec = [0;0;1];
r = norm(rvec);
sinphi = rvec(3)/r;
dadrJ2 = -(mu/r^3)*(eye(3)-(3/r^2)*(rvec*rvec'))-((3*mu*req^2*J2)/(2*r^5))*(5*((7*sinphi^2-1)/r^2)*(rvec*rvec')...
    -((10*sinphi)/r)*(rvec*kvec'+kvec*rvec')+(1-5*sinphi^2)*eye(3)+2*(kvec*kvec'));
dadrJ3 = 2.5*((mu*req^3)/req^3)*J3*(((63*sinphi^3-21*sinphi)/r^2)*(rvec*rvec')...
    -((21*sinphi^2-3)/r)*((kvec*rvec')+(rvec*kvec'))-(7*sinphi^3-3*sinphi)*eye(3)+6*sinphi*(kvec*kvec'));

dadr = dadrJ2+dadrJ3;
Amat = [zeros(3,3),eye(3);dadr,zeros(3,3)];
end