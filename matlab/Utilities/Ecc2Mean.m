function M = Ecc2Mean(E,e)

M=E-(e.*sin(E));

if M < 0
    M = M + 2*pi;
else
    M;
end