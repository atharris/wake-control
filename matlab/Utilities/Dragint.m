function dstate = Dragint(state,DParams)

%Drag Model Parameters
re = 6378136.3;
rho0 = 3.614e-13;
r0 = 700000+re;
H = 88667;

%Relative Velocity Calculation (Assumes Co-Rotating Atmosphere)
wE = [0;0;7.2921158553E-5];
vATM = cross(wE,state(1:3));
vRel = vATM-state(4:6);

%Density Model
if length(DParams)<4
    rho = rho0*exp(-(norm(state(1:3))-r0)/H);
else
    rho = DParams{4};
end

dstate = 0.5*rho*((DParams{1}*DParams{3})/DParams{2})*norm(vRel)*vRel;