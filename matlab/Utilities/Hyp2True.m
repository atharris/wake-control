function True = Hyp2True(H,e)

nu_s = (-sinh(H)*sqrt(e^2-1))/(1-e*cosh(H));
nu_c = (cosh(H)-e)/(1-e*cosh(H));

True = atan2(nu_s,nu_c);

if True < 0
    True = True + 2*pi;
else
    True;
end