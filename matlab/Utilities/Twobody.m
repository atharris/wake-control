function dstate = Twobody(state,mu)

dstate = -(mu/norm(state(1:3))^3)*state(1:3);