function [az,el] = ecef2sky(ecef_ref, ecef_obj)
    %Converts observer and object ECEF coordinates to azimuth and elevation relative to observer.
    %
    %Parameters
    %----------
    %ecef_ref : array of shape (3,) or (1,3)
    %    observer coordinate
    %ecef_obj : array of shape(N,3)
    %    object coordinates
    %
    %Returns
    %-------
    %output : array of shape(N,2)
    %    The objects' sky coordinatescoordinates
    %    (azimuth, elevation in degrees)
    %
    enu = ecef2enu(ecef_ref, ecef_obj);
    e = enu(1,:);
    n = enu(2,:);
    u = enu(3,:);
    az = atan2d(e, n);
    el = asind(u ./ sqrt(e.^2 + n.^2 + u.^2));
end

