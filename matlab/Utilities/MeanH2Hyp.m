function Hyp = MeanH2Hyp(M,e)

if e<1.6
    if -pi<M<0 || M>pi
        H(1)=M-e;
    else
        H(1)=M+e;
    end
else
    if e<3.6 && abs(M)>pi
        H(1)=M-sign(M)*e;
    else
        H(1)=M/(e-1);
    end
end

H(2)=H(1)+((M-e*sinh(H(1))+H(1))/(e*cosh(H(1))-1));

i=2;
while abs(H(i)-H(i-1))>10^-16
    H(i+1)=H(i)+((M-e*sinh(H(i))+H(i))/(e*cosh(H(i))-1));
    i=i+1;
end

Hyp=mod(H(i),2*pi);

if Hyp < 0
    Hyp = Hyp + 2*pi;
end

end

