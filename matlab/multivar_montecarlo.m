function results = multivar_montecarlo(func_inputs,mc_vars,main_function,varargin)

% Recursive function to implement multivariable montecarlo analysis
%
% multivar_montecarlo implements a nested for loop with the number of
% nested for loops (i.e. the recursion depth) dynamically specified by the
% inputs to the function.  Primarily designed for multi variable monte
% carlo analysis, the function will modify a single, scalar variable at
% each level of the nested loops, and pass a modified input structure into
% the next nested for loop.  The innermost for loop will execute the
% specified function using the input structure, as modified for the
% variables being varied.  The function does not support varying a
% component of a vector.  
%
% If the main function failes, the montecarlo routine will continue to run.
% The error runs and the error messages reported by the main function will
% be stored in the results structure, as described further below.  Also, if
% the main function fails on the final call to the main program, then the
% output will for that run will be set to the output from the final 
% successful run.
%
% Functional forms:
%   results = multivar_montecarlo(func_inputs,mc_vars,main_function)
%   results = multivar_montecarlo(func_inputs,mc_vars,main_function,...
%               verbose_level)
%   results = multivar_montecarlo(func_inputs,mc_vars,main_function,...
%               verbose_level,results)
%
% It is recommended to only use the first two functional forms.
%
% Inputs:
%
% func_inputs: a structure to be passed into the main function.  Func
% inputs must contain all of the required inputs that are not being varied
% by the function and can optionally include any or all variables being
% varied.  func_inputs will be the sole input to the main function.  A
% recursion structure is added to the inputs structure, allowing the main
% function to access recursion information, in particular to support
% specialized processing the first time the function is called.
%
% mc_vars: a structure array containing two fields, 'var' and 'range'.
% Each element of mc_vars should contain a string in the 'var' field which
% is the name of the scalar being varied, and a scalar or vector in the
% 'range' field containing the values over which to vary the scalar.
%
% main_function: a function handle to the main function to be called.  The
% functional form of the main function must be output =
% functionname(inputs).  The required elements of the inputs structure must 
% match func_inputs, with the addition of the monte carlo variables.  The
% output format has no requirements.
%
% verbose_level: optional input argument to control status display to the
% screen.  A verbose level of 0 will result in no status display.  A
% verbose level of 1 will display progress for each iteration of the
% outermost loop.  A verbose level of 2 or higher will display progress for
% each iteration of the innermost loop.
%
% results: optional input argument for a structure to which the results
% will be added.  If passed as an input argument, this structure should not
% include fields named 'inputs', 'output', or 'recursion', or unexpected
% behavior may occur. Best practice is to not use this input argument.
%
% Outputs:
%
% results: a structure with three fields:
%   'output' is an m x n x p...structure array where m, n, p... are the 
%   sizes of the range vectors of the monte carlo variables, from first to 
%   last (i.e. outer to inner for loops).  Each element of output is the 
%   output structure of a single call to the main function
%   'input' is an m x n x p...structure array.  Each element is a copy of
%   the input structure sent to the main function
%   'recursion' is a structure containing variables used in the recurcion
%   process.  The structure will also contain an error_runs, an error 
%   vector listing the run numbers where the main function failed, and 
%   error_info, a cell array containing the Matlab error messages for all 
%   of the runs (blank if the run was successful).
%
% Examples:
%
% cylinder_vars(1).var = 'radius';
% cylinder_vars(1).range = [1:0.1:2];
% cylinder_vars(2).var = 'height';
% cylinder_vars(2).range = [10 50 100];
% inputs.plot_color = 'red';
% function_to_vary = @draw_cylinder;
% results = multivar_montecarlo(inputs,cylinder_vars,function_to_vary);

% Developer: Dr. Kenneth Horneman
% Date: July 7, 2016
%
% Revision history:
% 7/12/2016: Added recursion structure to the input structure for the
% main function call.  Added progress report show statement.  Adde varargin
% for verbose level to control progress display. Added error check for
% nargin, and try catch with error for main function call.

% nargin will only be 3 if user passes no results structure.  nargin will
% always be 4 when function has been called recursively

switch nargin
    case {1,2}
        error('Insufficient input variables to multivar_montecarlo.');
    case 3
        verbose_level = 1;
        results = [];
    case 4
        verbose_level = varargin{1};
        results = [];
    otherwise
        verbose_level = varargin{1};
        results = varargin{2};
end
        
        
% results.recursion values initialized if they aren't present, i.e. at
% first call to function, otherwise level counter increased by one

if isfield(results,'recursion')
    results.recursion.current_level = results.recursion.current_level + 1;
else
    results.recursion.final_level = length(mc_vars);
    results.recursion.current_level = 1;
    results.recursion.total_runs = 0;
    results.recursion.error_runs = [];
    final_run = 1;
    for i = 1:length(mc_vars)
        final_run = final_run * length(mc_vars(i).range);
    end
    results.recursion.final_run = final_run;
end

current_level = results.recursion.current_level;
final_level = results.recursion.final_level;

current_var = mc_vars(current_level).var;
current_range = mc_vars(current_level).range;

for current_value = current_range
    % set value of monte carlo variable, setsubfield supports multilevel
    % structure, but not vector elements
    func_inputs = setsubfield(func_inputs,current_var,current_value);
    % If on innermost loop, execute function, store inputs and outputs
    if current_level == final_level
        results.recursion.total_runs = results.recursion.total_runs + 1;
        func_inputs.recursion = results.recursion;
        currentind = results.recursion.total_runs;
        results.inputs(currentind) = func_inputs;
        try
            output = main_function(func_inputs);
            results.output(currentind) = output;
        catch functionerror
            results.recursion.error_runs = [results.recursion.error_runs ...
                results.recursion.total_runs];
            results.recursion.error_info{currentind} = getReport(functionerror);
            if verbose_level > 0
                warning_struc = warning('QUERY', 'backtrace');
                warning off backtrace
                warning('Main function call failed, run number = %i\n',...
                    results.recursion.total_runs);
                if strcmp(warning_struc.state,'on')
                    warning on backtrace
                end
                if verbose_level > 1
                    results.recursion.error_info{currentind}
                end
            end
        end
    else % not on innermost loop, execute recursion
        results = multivar_montecarlo(func_inputs,mc_vars,main_function,verbose_level,results);
    end
    
    if verbose_level > 2
        if current_level == final_level
            fprintf(1,'Run %d complete.\n\n',results.recursion.total_runs);
        end
    elseif verbose_level > 0
        if current_level == 1
            fprintf(1,'Run %d complete.\n\n',results.recursion.total_runs);
        end
    end
end

% If done looping, and on level one, then recursion is complete, so
% reformat results to be m x n x p... Results are stored internally as a
% single dimension vector

if current_level == 1
    reshape_dimensions = [];
    inner_to_outer = final_level:-1:1;
    if length(results.output) < results.recursion.total_runs
        results.output(results.recursion.total_runs) = results.output(end);
    end
    for index = inner_to_outer
        reshape_dimensions = [reshape_dimensions length(mc_vars(index).range)];
    end
    results.inputs = reshape(results.inputs,reshape_dimensions);
    results.inputs = permute(results.inputs,inner_to_outer);
    results.output = reshape(results.output,reshape_dimensions);
    results.output = permute(results.output,inner_to_outer);
    
end

% Reduce current level by one before returning to next level up.

results.recursion.current_level = results.recursion.current_level - 1;

end
