function [results,  debug] = wake_control_analysis(statel,inputs)
%wake_control_analysis This function implements the full wake
%controllability analysis for the purpose of analyzing the sensitivity of
%wake-based control to various input parameter selections.
%   Inputs:
%   inputs - struct - contains all input values.
%
%   Outputs:
%   outputs - struct - contains all output parameters.
%   debug - struct - contains relevant information for debugging purposes.

[~, ~, ~, ~, A_aero, v_aero] = A_aero_full(inputs.chief_geometry, inputs.dep_geometry, inputs.atmo_density, inputs.mean_mot, inputs.chief_rad);
A_full = A_aero;

n = inputs.n;
inputs.ad = max(abs(v_aero));
ACoul = ACoul_Linearized(statel,inputs);
BCoul = BCoul_Linearized(statel,inputs);
A_full(4:end, 1:3) =+ ACoul;
B_full = [zeros(size(BCoul)); BCoul];

ctrl_mat = ctrb(A_full, B_full);

ctrl_subspace_size = rank(ctrl_mat);

[Q_ctrl, ~] = qr(ctrl_mat);

ctrl_eig_vec = Q_ctrl(:,1:ctrl_subspace_size);

state_gain = 10 * eye(6);
ctrl_gain = 0.001 * eye(n);

ct_sys = ss(A_full,B_full,eye(6),zeros(6,n));
debug.ct_sys = ct_sys;
try
    lqr_gain = lqr(A_full, B_full, state_gain, ctrl_gain);
    stab_A = A_full - B_full * lqr_gain;
    [stab_evec, stab_eval] = eig(stab_A);
catch
    warning(sprintf('Could not compute LQR control gain for n=%d, Vf = %d, mf = %d, r_c = %d.', inputs.n,inputs.Vf,inputs.mf, inputs.chief_rad))
    lqr_gain = NaN * ones(n,6);
    stab_evec = NaN * ones(6,6);
    stab_eval = NaN * ones(6,6);
end

results.lqr_gain = lqr_gain;
results.ctrl_subspace_size = ctrl_subspace_size;
results.ctrl_eig_vec = ctrl_eig_vec;
results.stab_eval = diag(stab_eval);
results.stab_evec = stab_evec;
results.Vvec = inputs.Vnom;

end

