function [ skewMat ] = skew(vec)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

vec1 = vec(1);
vec2 = vec(2);
vec3 = vec(3);

skewMat = [0 -vec3 vec2;
        vec3 0 -vec1;
        -vec2 vec1 0];

end

