
for ind = 1:length(sweep_vals(1).range)
    for ind2 = 1:length(sweep_vals(2).range)
        max_eval_mat(ind,ind2) = max(results.output(ind,ind2).stab_eval);
        min_eval_mat(ind,ind2) = min(results.output(ind,ind2).stab_eval);
    end
end

% figure()
% for plot_ind = 1:length(sweep_vals(2).range)
%     plot(sweep_vals(1).range, max_eval_mat(plot_ind,:))
%     hold on
%     plot(sweep_vals(1).range, min_eval_mat(plot_ind,:));
% end

[x,y] = meshgrid(sweep_vals(1).range, sweep_vals(2).range);

figure()
surf(x', y', real(min_eval_mat));

xlabel(sweep_vals(1).var);
ylabel(sweep_vals(2).var);
