function S = MSMElastance(positions,radii)

n = length(positions(1,:));
kc = 8.99e9;

% S = (1./radii).*eye(n);
% 
% for ii = 1:n
%     for jj = ii:n
%         if jj == ii
%             continue
%         else
%             S(ii,jj) = 1/sqrt((positions(:,ii)-positions(:,jj))'*(positions(:,ii)-positions(:,jj)));
%             S(jj,ii) = S(ii,jj);
%         end
%     end
% end
% 
% S = kc*S;
SPHS = [positions;radii*ones(1,n)];
sphsn = repmat(SPHS(1:3,:),1,1,n);
diffV = sphsn - permute(sphsn,[1 3 2]);
diffV(1,:,:) = squeeze(diffV(1,:,:)) + diag(SPHS(4,:));
S = kc*squeeze(1./sqrt(diffV(1,:,:).^2 + diffV(2,:,:).^2 + diffV(3,:,:).^2));