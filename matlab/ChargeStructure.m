function positions = ChargeStructure(n,rad)

a = ((n-2)*180)/n;
b = 180-a;

nvec = 0:1:n-1;
positions = rad*[cosd(nvec*b);zeros(1,n);sind(nvec*b)];

