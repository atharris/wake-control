%%  

mu =  3.986004415e14; %kg^3/m^2
aero_params_init;
inputs.chief_rad = 6700e3; %;m
earth_rad = 6378.137e3;
inputs.atmo_density = earthExpAtmo(inputs.chief_rad); % kg/m^3
inputs.mean_mot = sqrt(mu/inputs.chief_rad^3);

inputs.n = 10;
inputs.radii = 0.01;
inputs.rad_cs = 1;
inputs.mf = 1;
inputs.rf = [0;-1;0];
inputs.Vf = 450;

inputs.chief_geometry.area = 0.5; %    m^2
inputs.chief_geometry.drag_coeff = 2.2; %  m^2;
inputs.chief_geometry.mass = 1000; %    kg
inputs.chief_geometry.beta = chief_geometry.area * chief_geometry.drag_coeff/ chief_geometry.mass;

inputs.dep_geometry.area = 0.0; %    m^2
inputs.dep_geometry.drag_coeff = 2.2; %  m^2;
inputs.dep_geometry.beta = dep_geometry.area * dep_geometry.drag_coeff;

[~, ~, ~, ~, A_aero, v_aero] = A_aero_full(inputs.chief_geometry, inputs.dep_geometry, inputs.atmo_density, inputs.mean_mot, inputs.chief_rad);
inputs.ad = max(abs(v_aero));
Vfm = Vf_Min(inputs);
Vf_range = logspace(log10(Vfm),3,10);

sweep_vals(1).var='Vf';
sweep_vals(1).range = Vf_range;
sweep_vals(2).var = 'n';
sweep_vals(2).range = 12:20;

results = multivar_montecarlo(inputs, sweep_vals, @wake_control_analysis);