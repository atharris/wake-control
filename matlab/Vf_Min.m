function Vfm = Vf_Min(statel,inputs)
%% Introduction

%This code provides the minimum follower voltage for a given system
%geometry and drag acceleration. The mutual capacitance between the follower and charge structure
%can be such that as the charge structure charge increases, the follower
%charge falls off as (Q_chargestructure)^n where n>1. Therefore, the
%Coulomb repulsion cannot generate a sufficiently large acceleration to
%balance drag.

% n : The total number of spheres in the system (including the follower)

% radii : A vector of the radii of each of the n spheres in the model

% rad_cs : The radius of the charge structure (i.e. the distance of each
% charge structure sphere to the point [0,0,0])

% rf : The vector describing the position of the follower

% mf : The mass of the follower

% ad : The acceleration magnitude to be balanced

% Suggested Parameters
% n = 8;
% radii = 0.01;
% rad_cs = 1;
% rf = [0;-1;0];
% mf = 1;
% Vf = 100;
% ad = 2.9230e-08;

%% Code

%Break variables out of struct
n = inputs.n;
radii = inputs.radii;
rad_cs = inputs.rad_cs;
rf = ECI2ChiefHill(statel,inputs.rf);
mf = inputs.mf;
ad = inputs.ad;

kc = 8.99e9;

%Create a vector of positions of each sphere [follower,charge structure]
positions = [rf(1:3),ChargeStructure(n-1,rad_cs)];
distances = positions(:,1)-positions;
distances(:,1) = [];

%Calculate capacitance matrix
S = MSMElastance(positions,radii);
C = inv(S);

%% Variable Definitions

alpha = C(1,1);
beta = sum(C(1,2:end));
gamma = 0;
delta = 0;

for ii = 2:n
    gamma = gamma + (C(1,ii)/norm(distances(:,ii-1))^2);
    for jj = 2:n
        delta = delta + (C(ii,jj)./vecnorm(distances(:,ii-1))^2);
    end
end

Vfm = sqrt(-((4*ad*mf*beta*delta)/(kc*(beta*gamma-alpha*delta)^2)))+1e-16;

