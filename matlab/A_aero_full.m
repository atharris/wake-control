function [A_pp, A_pv, A_vp, A_vv, A_aero, v_disturbance] = A_aero_full(chief_geometry, dep_geometry, atmo_density, n, r_c)
%A_aero_full Function to compute the linearized relative dynamics of two
%spacecraft from atomspheric drag.

A_pp = zeros(3,3);
A_pv = eye(3);
A_vp = [3*n^2, 0, 0;
        0, 0, 0;
        0, 0, -n^2];
A_vv = [-0.5 * dep_geometry.beta * atmo_density * n * r_c, 2*n, 0;
        -2*n, -dep_geometry.beta * atmo_density * n * r_c, 0;
        0, 0, -0.5 * (dep_geometry.beta * atmo_density * r_c * n)];
    
A_aero = [A_pp, A_pv;
          A_vp, A_vv];

      
v_disturbance = [zeros(3,1);
                 0;
                 -n^2 * r_c^2 * 0.5 * (chief_geometry.beta * atmo_density - dep_geometry.beta * atmo_density);
                 0];
end
