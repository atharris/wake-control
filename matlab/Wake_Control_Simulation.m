%% Wake Control Simulation Script
set(0,'defaulttextinterpreter','latex')
set(0,'defaultAxesFontSize',16)

%Setup input vector
mu =  3.986004415e14; %kg^3/m^2
aero_params_init;
inputs.chief_rad = 6700e3; %;m
earth_rad = 6378.137e3;
inputs.atmo_density = earthExpAtmo(inputs.chief_rad); % kg/m^3
inputs.mean_mot = sqrt(mu/inputs.chief_rad^3);

inputs.n = 10;
inputs.radii = 0.01;
inputs.rad_cs = 1;
inputs.mf = 1;
% inputs.rf = [0;-1;0];
inputs.Vf = 10;

inputs.chief_geometry.area = 0.5; %    m^2
inputs.chief_geometry.drag_coeff = 2.2; %  m^2;
inputs.chief_geometry.mass = 1000; %    kg
inputs.ml =  inputs.chief_geometry.mass;
inputs.chief_geometry.beta = chief_geometry.area * chief_geometry.drag_coeff/ chief_geometry.mass;

inputs.dep_geometry.area = 0.0; %    m^2
inputs.dep_geometry.drag_coeff = 2.2; %  m^2;
inputs.dep_geometry.beta = dep_geometry.area * dep_geometry.drag_coeff;

[~, ~, ~, ~, A_aero, v_aero] = A_aero_full(inputs.chief_geometry, inputs.dep_geometry, inputs.atmo_density, inputs.mean_mot, inputs.chief_rad);
inputs.ad = max(abs(v_aero));


%Setup initial conditions
d2r = pi/180;
oel0 = [inputs.chief_rad,0,0,0,20,10].*[1,1,d2r,d2r,d2r,d2r];
statel0 = COE2RV(oel0);
% oef = oeL-[0,0,0,0,0,0.0000075*d2r];
statef0 = ChiefHill2ECI(statel0,[0;1;0;0;0;0]);
inputs.rfnom = statef0;
inputs.rf = statef0;
diffcheck = statef0-statel0;
diffHcheck = ECI2ChiefHill(statel0,statef0);
period = 2*pi*sqrt(inputs.chief_rad^3/mu);

inputs.Vnom0 = Vnominal(statel0,inputs);
inputs.Vnom = inputs.Vnom0;
control = wake_control_analysis(statel0,inputs);
inputs.LQR_Gain = control.lqr_gain;

%Setup integrator inputs
params.leader.Acs = inputs.chief_geometry.area;
params.leader.m = inputs.ml;
params.leader.CD = inputs.chief_geometry.drag_coeff;
params.follower.Acs = 0;
params.follower.m = inputs.mf;
params.follower.CD = inputs.dep_geometry.drag_coeff;
params.mu = mu;
params.rhoATM = inputs.atmo_density;

state0 = [statel0;statef0];

tvec = 0:1:period*10;

toleranceoptions = odeset('Reltol',1e-14,'Abstol',1e-16*ones(size(state0)));
[time,vecout] = ode45(@(t,y) dstate_wakecontrol(t,y,params,inputs),tvec,state0,toleranceoptions);
state = vecout';

statel = state(1:6,:);
statef = state(7:12,:);


%% Plotting

diff = statef-statel;
for ii = 1:length(diff(1,:))
    [stateH(:,ii)] = ECI2ChiefHill(statel(:,ii),statef(:,ii));
    oel(:,ii) = RV2COE(statel(:,ii));
    oef(:,ii) = RV2COE(statef(:,ii));
end

% figure
% hold on
% plot3Dorbit(statel)
% plot3Dorbit(statef)
% legend('Leader','Follower')

figure
plot(time/period,statel(1,:),time/period,statel(2,:),time/period,statel(3,:))
legend('X','Y','Z')
xlabel('Time (Period)')
ylabel('Positions (m)')
title('ECI Frame Leader Position')

figure
plot(time/period,statel(4,:),time/period,statel(5,:),time/period,statel(6,:))
legend('V_X','V_Y','V_Z')
xlabel('Time (Period)')
ylabel('Velocity (m/s)')
title('ECI Frame Leader Velocity')

figure
plot(time/period,stateH(1,:),time/period,stateH(2,:),time/period,stateH(3,:))
legend('X','Y','Z')
xlabel('Time (Period)')
ylabel('Positions (m)')
title('Hill Frame Positions')

figure
plot(time/period,stateH(4,:),time/period,stateH(5,:),time/period,stateH(6,:))
legend('V_X','V_Y','V_Z')
xlabel('Time (Period)')
ylabel('Velocity (m/s)')
title('Hill Frame Velocity')

diffMag = vecnorm(stateH(1:3,:));
diffMagZero = diffMag-diffMag(1);
diffPerc  = 100*(diffMag-diffMag(1))/diffMag(1);

figure
plot(time/period,diffPerc)
xlabel('Time (Period)')
ylabel('Separatation Percent Difference')
title('Hill Frame Separation Percent')

figure
plot(time/period,diffMag)
xlabel('Time (Period)')
ylabel('Difference Magnitude (m)')
title('Hill Frame Separation Distance')

figure
plotOE(time,oel)

figure
plotOE(time,oef)
