function [ atmo_dens ] = earthExpAtmo( eci_pos )
%earthExpAtmo calculates atmospheric density given an ECI position in m

scaleHeight = 8.5e3;
baseDens = 1.217;
earthRad = 6378.137e3;
orbitRad = norm(eci_pos);
alt = orbitRad - earthRad;

atmo_dens = baseDens * exp(-alt / scaleHeight);

end

