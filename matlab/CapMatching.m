function dC = CapMatching(positions,radii,scalarcap)

dC = sum(sum(inv(MSMElastance(positions,radii))))-scalarcap;