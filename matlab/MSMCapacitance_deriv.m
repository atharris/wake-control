function Cp = MSMCapacitance_deriv(positions,radii)

S = MSMElastance(positions,radii);
C = inv(S);

Sp = MSMElastance_deriv(positions);

Cp = zeros(size(Sp));

for ii = 1:3
    Cp(:,:,ii) = -C*Sp(:,:,ii)*C;
end

end