function Vvec = Vnominal(statel,inputs)
%% Introduction

%This code provides the necessary voltage on the charge structure to null
%the relative acceleration (ad) between the leader and follower for a given
%follower potential. It is only valid for a charge structure in which all 
%spheres are at the same potential. The point of this code is to assess the
%feasibility of the technique and to determine the voltage about which to 
%linearize the equations of motion.

% n : The total number of spheres in the system (including the follower)

% radii : A vector of the radii of each of the n spheres in the model

% rad_cs : The radius of the charge structure (i.e. the distance of each
% charge structure sphere to the point [0,0,0])

% rf : The vector describing the position of the follower

% mf : The mass of the follower

% ad : The acceleration magnitude to be balanced

% Suggested Parameters
% n = 8;
% radii = 0.01;
% rad_cs = 1;
% rf = [0;-1;0];
% mf = 1;
% Vf = 100;
% ad = 2.9230e-08;

%% Code

%Break variables out of struct
n = inputs.n;
radii = inputs.radii;
rad_cs = inputs.rad_cs;
rf = ECI2ChiefHill(statel,inputs.rf);
mf = inputs.mf;
Vf = inputs.Vf;
ad = inputs.ad;

kc = 8.99e9;

%Check if Vf is large enough
Vfm = Vf_Min(statel,inputs);

if Vf<Vfm
    disp(strcat('Vf must be larger than',{' '},num2str(Vfm,16),'V',{' '},'for the given parameter set'))
end

%Create a vector of positions of each sphere [follower,charge structure]
positions = [rf(1:3),ChargeStructure(n-1,rad_cs)];

%Calculate capacitance matrix
S = MSMElastance(positions,radii);
C = inv(S);

%Determine the relative positions between the follower and the charge
%structure spheres
r_rel = positions(:,1)-positions;

%Come up with parameters to simplify the math
a = C(1,1)*Vf;
b = sum(C(1,2:end));
c = 0;
d = 0;

for ii = 2:n
    c = c+C(ii,1)/norm(r_rel(:,ii))^2;
    d = d+sum(C(ii,2:end))/norm(r_rel(:,ii))^2;
end

%Create 
poly(1) = (kc/mf)*b*d;
poly(2) = (kc/mf)*(Vf*b*c+a*d);
poly(3) = ((kc/mf)*Vf*a*c)-ad;

Vcs = roots(poly);

Vvec = [Vf;min(Vcs)*ones(n-1,1)];

% Additional Check
% Ecs = 0;
% 
% for ii = 2:n
%     Ecs = Ecs+((kc*C(ii,:)*Vvec)/norm(r_rel(:,ii))^2);
% end
% da = ((C(1,:)*Vvec)*Ecs/mf)-ad