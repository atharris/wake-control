function [aCl,aCf] = ECI_CoulombAccel(coords,inputs)
%% Introduction

%This code provides the Coulomb acceleration in the HCW frame between a
%charged leader and follower.

% n : The total number of spheres in the system (including the follower)

% radii : A vector of the radii of each of the n spheres in the model

% rad_cs : The radius of the charge structure (i.e. the distance of each
% charge structure sphere to the point [0,0,0])

% rf : The vector describing the position of the follower

% mf : The mass of the follower

% Vcs : The voltage on the charge structure, (n-1)x1 vector

% Vf : The voltage on the follower

% Suggested Parameters
% n = 8;
% radii = 0.01;
% rad_cs = 1;
% rf = [0;-1;0];
% mf = 1;
% Vcs = [100;100*ones(n-2,1)];
% Vf = 100;
% ad = 2.9230e-08;

%% Code

%Break variables out of struct
n = inputs.n;
radii = inputs.radii;
rad_cs = inputs.rad_cs;
rf = inputs.rf;
mf = inputs.mf;
ml = inputs.ml;
V = inputs.Vnom;

kc = 8.99e9;

%Create a vector of positions of each sphere [follower,charge structure]
positions = [rf(1:3),ChargeStructure_ECI(n,rad_cs,coords)];

%Calculate capacitance matrix
S = MSMElastance(positions,radii);
C = inv(S);

%Calculate the charge on all spheres
Q = C*V;

%Determine the relative positions between the follower and the charge
%structure spheres
r_rel = positions(:,1)-positions;

%Calculate electric field of charge structure
E = kc*sum(((Q(2:end)./vecnorm(r_rel(:,2:end))'.^3)'.*r_rel(:,2:end)),2);

%Calculate acceleration of the follower due to Coulomb force
aCf = (Q(1)*E)/mf;
aCl = -(mf/ml)*aCf;