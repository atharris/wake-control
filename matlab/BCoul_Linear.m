function BCoul = BCoul_Linear(inputs)
%% Introduction

%This code provides the A matrix resulting from linearization of the
%Coulomb force.

% n : The total number of spheres in the system (including the follower)

% radii : A vector of the radii of each of the n spheres in the model

% rad_cs : The radius of the charge structure (i.e. the distance of each
% charge structure sphere to the point [0,0,0])

% rf : The vector describing the position of the follower

% mf : The mass of the follower

% Vnom : The nominal voltage about which the system is linearized

% Suggested Parameters
% n = 8;
% radii = 0.01;
% rad_cs = 1;
% rf = [0;-1;0];
% mf = 1;
% Vf = 100;
% ad = 2.9230e-08;

%% Code

%Break variables out of struct
n = inputs.n;
radii = inputs.radii;
rad_cs = inputs.rad_cs;
rf = inputs.rf;
mf = inputs.mf;
Vf = inputs.Vf;
Vnom = inputs.Vnom;
ad = inputs.ad;

kc = 8.99e9;

%Create a vector of positions of each sphere [follower,charge structure]
positions = [rf,ChargeStructure(n-1,rad_cs)];

%Calculate capacitance matrix (simplified assuming mutual capacitance
%between the leader and follower is vanishingly small
S = MSMElastance(ChargeStructure(n-1,rad_cs),radii(2:end,1));
C_LR = inv(S);
C = [radii(1,1)/kc,0,0;0,C_LR(1,:);0,C_LR(2,:)];

%Determine the relative positions between the follower and the charge
%structure spheres
r_rel = positions(:,1)-positions;

%Come up with parameters to make the math simpler
B1 = zeros(3,1);
B2 = zeros(3,3);

for ii = 2:n
    B1 = B1+(dot(C(:,ii),Vnom)/norm(r_rel(:,ii))^3)*r_rel(:,ii);
    B2 = B2+((r_rel(:,ii)*C(:,ii)')/norm(r_rel(:,ii))^3);
end

%Final A Matrix Calculation
BCoul = (kc/mf)*(B1*C(:,1)'+B2*dot(C(1,:),Vnom));

end