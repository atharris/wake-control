function Sp = MSMElastance_deriv(positions)

n = length(positions(1,:));
kc = 8.99e9;

Sp = zeros(n,n,3);

r_rel = permute(positions(:,1)-positions,[3,2,1]);

for ii = 2:n
    Sp(ii,1,:) = -kc*r_rel(:,ii,:)/norm(squeeze(r_rel(:,ii,:)))^3;
    Sp(1,ii,:) = Sp(ii,1,:);
end
end
    