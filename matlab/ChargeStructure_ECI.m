function positions_ECI = ChargeStructure_ECI(n,rad,statel)

a = ((n-2)*180)/n;
b = 180-a;

nvec = 0:1:n-1;
positions = rad*[cosd(nvec*b);zeros(1,n);sind(nvec*b)];

for ii = 1:n-1
    positions_ECI(:,ii) = ChiefHill2ECI(statel,[positions(:,ii);0;0;0]);
end

positions_ECI(4:6,:) = [];

