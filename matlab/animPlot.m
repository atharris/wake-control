% 
% figure('pos',[5 5 800 600])
% linePlt= plot3(squeeze(nlsRelState(7,sampInd,1)), squeeze(nlsRelState(8,sampInd,1)), squeeze(nlsRelState(9,sampInd,1)));
% hold on
% attPlot = quiver3(squeeze(nlsRelState(7,sampInd,1)), squeeze(nlsRelState(8,sampInd,1)), squeeze(nlsRelState(9,sampInd,1)), ...
% squeeze(rotNormal(1,sampInd,1)), squeeze(rotNormal(2,sampInd,1)), squeeze(rotNormal(3,sampInd,1)),5);
% %quiver3(squeeze(nlsRelState(7,sampInd,1)), squeeze(nlsRelState(8,sampInd,1)), squeeze(nlsRelState(9,sampInd,1)),...
% %     tnrRelDragForce(1,1)', tnrRelDragForce(2,1)', tnrRelDragForce(3,1)');
% 
% markerStart = plot3(squeeze(nlsRelState(7,sampInd,1)), squeeze(nlsRelState(8,sampInd,1)), squeeze(nlsRelState(9,sampInd,1)),'r*');
% markerCurr = plot3(squeeze(nlsRelState(7,sampInd,1)), squeeze(nlsRelState(8,sampInd,1)), squeeze(nlsRelState(9,sampInd,1)),'g*');
% markerFin = plot3(0,0,0,'b*');
% legend('Relative Trajectory','Normal Vector Direction','Initial Position','Initial Velocity')
% ax = gca;
% ax.XLimMode = 'manual';
% ax.YLimMode = 'manual';
% ax.ZLimMode = 'manual';
% 
% ax.XLim = [min(nlsRelState(7,sampInd,:)),max(nlsRelState(7,sampInd,:))];
% ax.YLim = [-10,10];
% ax.ZLim = [min(nlsRelState(9,sampInd,:)),max(nlsRelState(9,sampInd,:))];
% 
% xlabel('TNR X (m)','fontsize',smallFont)
% ylabel('TNR Y (m)','fontsize',smallFont)
% zlabel('TNR Z (m)','fontsize',smallFont)
% %title('TNR Relative Position, Plate 1 Angle, Drag Force','fontsize',largeFont)
% position = [-4 -4 8 8];
% set(gcf, 'PaperPosition', position);
% 
% view([-150,10])
% 
% nonlinMovie(length(tvec)) = struct('cdata',[],'colormap',[]);
% 
% for timeInd = 1:length(tvec)
%     
%     
%     linePlt.XData = squeeze(nlsRelState(7,sampInd,1:timeInd));
%     linePlt.YData = squeeze(nlsRelState(8,sampInd,1:timeInd));
%     linePlt.ZData = squeeze(nlsRelState(9,sampInd,1:timeInd));
%     
%     markerCurr.XData = squeeze(nlsRelState(7,sampInd,timeInd));
%     markerCurr.YData = squeeze(nlsRelState(8,sampInd,timeInd));
%     markerCurr.ZData = squeeze(nlsRelState(9,sampInd,timeInd));
%     
%     attPlot.XData = squeeze(nlsRelState(7,sampInd,timeInd));
%     attPlot.YData = squeeze(nlsRelState(8,sampInd,timeInd));
%     attPlot.ZData = squeeze(nlsRelState(9,sampInd,timeInd));
%     
%     attPlot.UData = squeeze(rotNormal(1, sampInd, timeInd));
%     attPlot.VData = squeeze(rotNormal(2,sampInd,timeInd));
%     attPlot.WData = squeeze(rotNormal(3,sampInd,timeInd));
%     
%     markerFin.XData = 0;
%     markerFin.YData = 0;
%     markerFin.ZData = 0;
%     
%     grid on
%     drawnow
%     ax = gca; 
%     ax.Units = 'pixels';
%     pos = ax.Position;
%     ti = ax.TightInset;
%     rect = [-ti(1), -ti(2), pos(3)+ti(1)+ti(3), pos(4)+ti(2)+ti(4)];
%     
%     nonlinMovie(timeInd) = getframe(gca, rect);
% 
% end
% 
% for timeInd = 1:length(tvec)
%     
%     
%     linePlt.XData = squeeze(mpcNlsRelState(7,sampInd,1:timeInd));
%     linePlt.YData = squeeze(mpcNlsRelState(8,sampInd,1:timeInd));
%     linePlt.ZData = squeeze(mpcNlsRelState(9,sampInd,1:timeInd));
%     
%     markerCurr.XData = squeeze(mpcNlsRelState(7,sampInd,timeInd));
%     markerCurr.YData = squeeze(mpcNlsRelState(8,sampInd,timeInd));
%     markerCurr.ZData = squeeze(mpcNlsRelState(9,sampInd,timeInd));
%     
%     attPlot.XData = squeeze(mpcNlsRelState(7,sampInd,timeInd));
%     attPlot.YData = squeeze(mpcNlsRelState(8,sampInd,timeInd));
%     attPlot.ZData = squeeze(mpcNlsRelState(9,sampInd,timeInd));
%     
%     attPlot.UData = squeeze(mpcRotNormal(1, sampInd, timeInd));
%     attPlot.VData = squeeze(mpcRotNormal(2,sampInd,timeInd));
%     attPlot.WData = squeeze(mpcRotNormal(3,sampInd,timeInd));
%     
%     markerFin.XData = 0;
%     markerFin.YData = 0;
%     markerFin.ZData = 0;
%     
%     grid on
%     drawnow
%     ax = gca; 
%     ax.Units = 'pixels';
%     pos = ax.Position;
%     ti = ax.TightInset;
%     rect = [-ti(1), -ti(2), pos(3)+ti(1)+ti(3), pos(4)+ti(2)+ti(4)];
%     
%     nonlinMovie(timeInd) = getframe(gca, rect);
% 
% end
% 
% v = VideoWriter('mpcTnrFilm');
% open(v);
% writeVideo(v, nonlinMovie)
% close(v);

%animTNRPlot(squeeze(mpcNlsRelState(:,1,:)), squeeze(mpcRotNormal(:,1,:)), 'workingMpcVideo', smallFont)
animTNRPlot(squeeze(nlsRelState(:,1,:)), squeeze(rotNormal(:,1,:)), 'bigNonlinearIc', smallFont)

