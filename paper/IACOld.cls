\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{IAC}[2014/07/10 v1.0 IAC document class]
\typeout{TYPESETTING in IAC conference paper...}

\LoadClass[10pt,twocolumn]{article}

%
%	Load in required packages
%
\RequirePackage{graphicx}
\RequirePackage{bm}
\RequirePackage{times}
\RequirePackage{overcite}
\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{subfigure}
\RequirePackage{mathabx}
\RequirePackage{mathtools}
\RequirePackage{color}
\RequirePackage[normalem]{ulem}
\RequirePackage{lastpage}
\RequirePackage[original,runin]{abstract}
\RequirePackage[total={6.5in,9in},top=1in, left=1in]{geometry}
\RequirePackage[colorlinks=false,hyperfootnotes=false]{hyperref}
\RequirePackage{url}
\RequirePackage{titling}
\RequirePackage[compact]{titlesec}
\RequirePackage[margin=0pt,font=small,labelfont=bf,labelsep=colon,indention=1.5em]{caption}
\RequirePackage{ragged2e}
\RequirePackage[perpage,ragged,hang]{footmisc}


%
%	setup header and footers
%
\RequirePackage{fancyhdr}
\fancyhead{}
\fancyfoot{}
\fancyhead[C]{\footnotesize \ConferenceNumber\ International Astronautical Congress, \ConferenceLocation. Copyright \copyright \ConferenceYear\ by the International Astronautical Federation. All rights reserved.}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyfoot[R]{Page \thepage \ of \pageref{LastPage}}
\fancyfoot[L]{\PaperNumber}


%
%	setup abstract formatting
%
\renewcommand{\abstracttextfont}{\normalfont\indent}
\setlength\abstitleskip{-10pt}
\renewcommand{\abstractname}{}


%
%	setup section title formatting
%
\renewcommand \thesection {\@Roman\c@section.}
\renewcommand\thesubsection{}
\renewcommand\thesubsubsection{}
\titleformat{\section}
  {\normalfont\uppercase}
  {\thesection}
  {0.5em}
  {\filcenter\uline}
\titleformat{\subsection}%
  {}{\thesubsection}{0.0em}%
  {\uline}{}
\titleformat{\subsubsection}%
  {}{\thesubsubsection}{1.5em}%
  {\uline}{}



%
%	setup maketitle formatting
%
\setlength{\droptitle}{-20pt}
\pretitle{\begin{center} \normalsize\PaperNumber \\ ~ \\ \MakeUppercase}
 \posttitle{\par\end{center}}
\preauthor{\normalfont}
\postauthor{\vspace{-5em}}
 \predate{\begin{center}\large}
 \postdate{\par\end{center}}

%
%	setup float environment spacing
%
\setlength\textfloatsep{5pt}
\setlength{\abovecaptionskip}{3pt}
\renewcommand{\figurename}{Fig.}

%
%	cancel the date formatting
%
\date{}


%
%	Rename the Bibliography section label
%
\renewcommand{\refname}{REFERENCES}

%
%	Miscellaneous definitions
%
\newcommand{\dg}{^{\circ}}
\DeclareMathOperator{\sgn}{sgn}

