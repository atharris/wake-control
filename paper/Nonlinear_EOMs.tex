Three perturbations are included in the simplified model used throughout this investigation: Two-body gravity, orbital drag, and Coulomb forces. The Hill frame is used with the origin at the center of the charge structure attached to the leader. Therefore, the relative accelerations of the follower relative to the leader (stationary in the Hill frame) are used throughout.

\begin{equation}
\label{EOMsimple}
\begin{aligned}
\leftexp{H}{\bm{\rho}} = \leftexp{N}{\bm{r}_{\text{L}}}-\leftexp{N}{\bm{r}_{{\text{F}}}}\\
\leftexp{H}{\bm{\dot{\rho}}} = \leftexp{N}{\bm{\dot{r}}_{\text{L}}}-\leftexp{N}{\bm{\dot{r}}_{{\text{F}}}}\\
\leftexp{H}{\bm{\ddot{\rho}}} = \leftexp{N}{\bm{\ddot{r}}_{\text{L}}}-\leftexp{N}{\bm{\ddot{r}}_{{\text{F}}}}
\end{aligned}
\end{equation}

Henceforth, the left superscript on $\bm{\rho}$ and its derivatives is suppressed. The state of the system is defined $\bm{X}=[\bm{\rho},\bm{\dot{\rho}}]^T$ and evolves according to the equation

\begin{equation}
\label{eq:state}
\bm{\dot{X}} = \bm{F}(\bm{X},\bm{V})
\end{equation}

where $\bm{F}$ is a non-linear vector function of the state and voltages $\bm{V}$ which, in this case, incorporates two-body gravity, atmospheric drag, and Coulomb forces. To apply linear control techniques, this equation must be linearized about some reference state and reference potential vector.

\begin{equation}
\label{linearizedstate}
\bm{\dot{X}}\approx \bm{F}(\bm{X}_0,\bm{V}_0)+\frac{\partial \bm{F}}{\partial \bm{X}}\bigg\rvert_{\bm{X}_0}(\bm{X}-\bm{X}_0)+\frac{\partial \bm{F}}{\partial \bm{V}}\bigg\rvert_{\bm{V}_0}(\bm{V}-\bm{V}_0)
\end{equation}

The value $\bm{F}(X_0,V_0)$ is the derivative of the state at the reference. Moving this term to the left side and using the $\Delta$ notation to indicate the different between the variables and their reference values gives the familiar state-space form of the equations.

\begin{equation}
\label{eq:statespaceform}
\bm{\Delta\dot{{X}}}=\frac{\partial \bm{F}}{\partial \bm{X}}\bigg\rvert_{\bm{X}_0}\bm{\Delta X}+\frac{\partial \bm{F}}{\partial \bm{V}}\bigg\rvert_{\bm{V}_0}\bm{\Delta V}
\end{equation}

\subsection{Coulomb Acceleration}

The Coulomb acceleration of the follower relative to the leader is calculated from the charge on the follower and the electric field of the leader. It is assumed throughout that the large mass of the leader relative to the follower results in a negligible acceleration of the leader resulting from repulsion with the follower.

\begin{equation}
\label{eq:Cforce}
\bm{F}_{\text{C}}(\bm{X},\bm{V}) = \frac{Q_{\text{F}}\bm{E}_{\text{L}}(\bm{X},\bm{V})}{m_{\text{F}}}
\end{equation}

The proximity of the follower to the charge structure on the leader means that a mutual capacitance exists between the two objects. This affect is described by the relation between the voltage and the charge on a given object.

\begin{equation}
\label{eq:Voltage}
V_i = k_c\frac{Q_i}{R_i}+k_c\sum_{j=1,j\neq i}^n \frac{Q_j}{r_{i,j}}
\end{equation}

where $k_{\text{C}}=8.99\times 10^9 {\text{Nm}}^2/{\text{C}}^2$ is Coulomb's constant, $R_i$ is the radius of the $i^{\text{th}}$ sphere, and $r_{i,j}$ is the distance between the $i^{\text{th}}$ and $j^{\text{th}}$ spheres. Throughout this paper, the subscript 1 refers to the follower and subscripts 2 through $n$ refer to the spheres on the charge structure. The relation above can be rewritten into a single matrix equation.
\begin{equation}
\left(\begin{array}{c}
V_1 \\ V_2 \\ \vdots \\ V_n
\end{array}\right)= k_c
\begin{bmatrix}
\nicefrac{1}{R_1} & \nicefrac{1}{r_{1,2}} & \hdots & \nicefrac{1}{r_{1,n}}\\
\nicefrac{1}{r_{2,1}} & \nicefrac{1}{R_2} & \hdots & \nicefrac{2}{r_{2,n}}\\
\vdots & \vdots	& \ddots & \vdots \\
\nicefrac{1}{r_{n,1}} & \nicefrac{1}{r_{n,2}} & \hdots & \nicefrac{1}{R_n}
\end{bmatrix}\left(\begin{array}{c}
Q_1 \\ Q_2 \\ \vdots \\ Q_n
\end{array}\right)\label{eq:ElastMat}
\end{equation}
Written in a more compact fashion

\begin{equation}
\bm{V} = [S]\bm{Q}
\end{equation}

Here, $[S]$ is the elastance matrix.\cite{Smythe:1968}  Another, well-known expression relating charge to voltage, $\mathbf{Q}=[C]\mathbf{V}$ indicates that the capacitance is the inverse of the elastance matrix.
\begin{equation}
\mathbf{Q}=[S]^{-1}\mathbf{V}\label{eq:invElast}
\end{equation}
This form is preferable, as the voltage is the control variable and the charge dictates the dynamics. The charge on the follower can be written as an inner product between the first row of the capacitance and the voltage vector. Following the convention establish above, the subscript F from equation \eqref{eq:Cforce} is replaced with 1 to indicate the row in the capacitance matrix.

\begin{equation}
\label{eq:QF}
Q_{\text{F}} = \bm{C}_1^T\bm{V}
\end{equation}

The electric field from the charge structure $\bm{E}_{\text{L}}$ at the position of the follower can be calculated by summing the individual fields from each of the spheres on the charge structure.

\begin{equation}
\label{eq:EL}
\bm{E}_L(\bm{X},\bm{V}) = k_{\text{C}}\sum_{i=2}^n\frac{\bm{C}_i^T\bm{V}}{r_{1,i}^3}\bm{r}_{1,i}
\end{equation}

Substituting Equations \eqref{eq:QF} and \eqref{eq:EL} into \eqref{eq:Cforce} yields the non-linear acceleration of the follower subject to the leader.

\begin{equation}
\label{CForce_Full}
\bm{F}_{\text{C}}(\bm{X},\bm{V}) = \frac{k_{\text{C}}}{m_{{\text{F}}}} \bm{C}_1^T\bm{V} \sum_{i=2}^n\frac{\bm{C}_i^T\bm{V}}{r_{1,i}^3}\bm{r}_{1,i}
\end{equation}